/*global module:false*/
module.exports = function(grunt) {
  grunt.loadNpmTasks('grunt-compass');
  grunt.initConfig({
    // pkg: '<json:package.json>',
    compass: {
      dev: {
        config: 'config.rb'
      }
    },
    meta: {
      name: 'Lead Refinement System',
      author: 'Michael Puckett, Nathan Mosher, T Jay Tipps',
      company: 'iostudio',
      // website: 'http://lrs.nationalguard.com',
      version: '0.1.0',
      banner: '/*! <%= meta.name %> - v<%= meta.version %> - ' +
        '<%= grunt.template.today("yyyy-mm-dd") %>\n' +
        '* <%= meta.author %>\n' +
        // '* <%= meta.website %>\n' +
        '* Copyright (c) <%= grunt.template.today("yyyy") %> ' +
        '<%= meta.company %>; Licensed MIT */'
    },
    lint: {
      files: [
        'grunt.js',
        'app/js/*.js',
        'app/js/app/*.js',
        'app/js/app/**/*.js',
        'app/js/app/**/**/*.js'
        
      ]
    },
    concat: {
      dist: {
        src: [
          '<banner:meta.banner>',
          // 'app/lib/foundation/modernizr.foundation.js',
          'app/lib/jquery.min.js',
          'app/lib/jquery-ui/core.js',
          'app/lib/jquery-ui/widget.js',
          'app/lib/jquery-ui/mouse.js',
          'app/lib/jquery-ui/datepicker.js',
          'app/lib/jquery-ui/slider.js',
          'app/lib/jquery-ui/timepicker.addon.js',
          'app/lib/spin.js',
          'app/lib/select2/select2.js',
          'app/lib/bootstrap/modal.js',
          'app/lib/moment.js',
          'app/lib/cookie.js',
          'app/lib/file-upload/iframe-transport.js',
          'app/lib/file-upload/file-upload.js',
          'app/lib/uri.js', // dev
          'app/lib/sinon-server.js', //dev
          'app/js/sinon.js', //dev
          'app/lib/angular/angular.js',
          'app/lib/angular.resource.js',
          'app/lib/angular.sanitize.js',
          'app/lib/angular.ui.js',
          // 'app/js/*.js',
          'app/js/*.js',
          'app/js/**/*.js',
          'app/js/**/**/*.js'
          // 'app/lib/chosen/chosen.jquery.js',
          //'<file_strip_banner:lib/**/*.js>'
        ],
        dest: 'app/app.js'
      }
    },
    min: {
      dist: {
        src: ['<banner:meta.banner>', '<config:concat.dist.dest>'],
        dest: 'app/app.min.js'
      }
    },
    compassWithGrowlNotifier: {
      dist: {
        src: 'test'
      }
    },
    watch: {
       scripts: {
          files: [
            'grunt.js',
            'app/js/*.js',
            'app/js/**/*.js',
            'app/lib/*.js',
            'app/lib/**/*.js'
          ],
          tasks: 'js'
       },
       stylesheets: {
        files: [
          'app/sass/*',
          'app/sass/**/*'
        ],
        tasks: 'css'
       }
    },
    compassPass: {
      anything: ''
    },
    concatPass: {
      anything: ''
    }
  });

  grunt.registerTask('js', 'concat concatPass');
  grunt.registerTask('css', 'compass compassPass');
  grunt.registerTask('default', 'concat concatPass compass compassPass min');

    // Growl Notifications

  var sys = require('sys');
  var exec = require('child_process').exec;
  function puts(error, stdout, stderr) { sys.puts(stdout) }

  // WARN AND FAIL
  ['warn', 'fatal'].forEach(function(level) {
    grunt.utils.hooker.hook(grunt.fail, level, function(opt) {
      exec('growlnotify -t "Grunt" -m "' + opt + '"', puts);
    });
  });

  var secretSuccessMessages = [
    'GO GO POWER RANGERS',
    'RAWWWWWR.',
    'Getcha sexy on.',
    'And knowing is half the battle.',
    'Porkchop sandwiches!',
    'You look nice today.',
    '+99999999',
    'Cool.',
    'Do you like me? Y/N',
    'Whatever bro.'
  ];

  var successMessages = [
    'That\'s awesome! You\'re awesome!',
    'PARTY.',
    'Rock on.',
    'Woo-hoo!',
    'Tubular dude.',
    'Suh-weeeeet!',
    'You win today.',
    'HIGH FIVE!',
    '<3',
    'Ballin\'.'
  ];

  var getSuccessMessage = function () {
    var isSecret = Math.floor(Math.random()*101) > 95,
        n = Math.floor(Math.random()*11),
        list = isSecret ? secretSuccessMessages : successMessages;
    return list[n];
  };

  // JS concat success
  grunt.task.registerMultiTask("concatPass", "growl js concat success", function() {
    exec('growlnotify -t "Grunt" -m "JAVASCRIPT compiled! ' + getSuccessMessage() + '"', puts);
  });

  // Compass compile success
  grunt.task.registerMultiTask("compassPass", "growl compass compile success", function() {
    exec('growlnotify -t "Grunt" -m "CSS compiled! ' + getSuccessMessage() + '"', puts);
  });
};
