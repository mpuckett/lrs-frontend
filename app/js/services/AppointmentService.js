lrsServices.service('AppointmentService', function (API, $http, $cacheFactory, NotificationService, SessionService, API) {

	var url = '/api/appointment',
		cache = $cacheFactory('appointments');

	// url = API.DOMAIN + url;

	return {
		query: function (request) {
			request = request || {};
			angular.extend(request, {
				method: 'GET',
				url: url,
				params: {
					sinon: true
				},
				transform: function (response) {
					angular.forEach(response.data, function (appointment) {
						cache.put(appointment.id, appointment);
					});
					return response;
				}
			});
			return $http(request); // cache?
		},
		create: function (request) {
			// Required: personID, stationRSID, time, type, location

			request = request || {};
			angular.extend(request, {
				method: 'POST',
				url: url,
				params: {
					sinon: true
				},
				data: angular.extend(request.data, {
					stationRSID: API.RSID // always?
				}),
				successMessage: {
					type: 'Add Contact',
					title: 'Contact Added',
					description: 'Congratulations! You successfully added an appointment.'
				}
			});
			return $http(request).then(function (response) {
				NotificationService.reportSuccess(response);
				return response;
			});
		}
	}
});