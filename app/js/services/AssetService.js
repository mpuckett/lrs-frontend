lrsServices.service('AssetService', function (API, $http, SessionService, $q) {

	// Asset API - for the list of all shared assets
	var assetUrl = '/api/asset';

	// Bookmarks API - for the recruiter's bookmarked assets
	var bookmarkUrl = '/api/recruiter/' + API.RSID + '/asset-favorite';

	var assetCache;
	var tagCache;

	var parseTags = function (assets) {
		return _.chain(assets).pluck('tags').flatten().uniq().value();
	};

	return {
		query: function (opts) {
			opts = opts || {};
			// attempt to return cache
			return assetCache && tagCache ?
				$q.when({data: { assets: assetCache, tags: tagCache }, cached: true}):
				$q.all([
					$http({
						method: 'GET',
						url: assetUrl,
						params: {
							sinon: true
						},
						data: opts.data
					}),
					$http({
						method: 'GET',
						url: bookmarkUrl,
						params: {
							sinon: true
						}
					})
				])
				.then(function (results) {
					var assets = results[0].data,
			    		bookmarks = results[1].data;
			    	// process data: add bookmarks where applicable
			        _.each(bookmarks, function (bookmark) {
						_.where(assets, {id: bookmark.id})[0].bookmark = true;
					});
					// save to cache
					assetCache = assets;
					// save tags to cache
					tagCache = parseTags(assets);

					return angular.extend(results[0], {
						tags: tagCache
					});
				});
		},
		create: function (request) {
			request = request || {};
			angular.extend(request, {
				method: 'POST',
				url: assetUrl,
				params: {
					sinon: true
				}
			});
			return $http(request);
		},
		save: function (request) {
			request = request || {};

			if (request.data) {
				// deconstruct model by removing reference to bookmark 
				delete request.data.bookmark; // TODO is this OK in ie8?
				// delete params.editable; // TODO do we need to remove all view data?
			}

			angular.extend(request, {
				method: 'PUT',
				url: assetUrl + '/' + request.id,
				params: {
					sinon: true
				}
			});
			return $http(request);
		},
		remove: function (request) {
			request = request || {};
			angular.extend(request, {
				method: 'DELETE',
				url: assetUrl + '/' + request.id,
				params: {
					sinon: true
				}
			});
			return $http(request);
		},
		toggleBookmark: function (request) {
			// match the current state of the model to add or delete
			var method = request.data && request.data.bookmark ? 'POST' : 'DELETE';
			// deconstruct model by only sending the ID

			request = request || {};
			angular.extend(request, {
				method: method,
				url: bookmarkUrl + '/' + request.id,
				params: {
					sinon: true
				},
				data: {
					id: request.id
				}
			});
			return $http(request);
		}
	}
});