lrsServices.service('SessionService', function (RecruiterService, $q, $route, $rootScope, $location, API, $http) {

    // var url = API.URL + 'session';
    var url = '/api/session';
    var retrievePasswordUrl = '/api/retrieve-password';
    var registerUrl = '/api/register';

    /*
     * ROUTES
     */

    // routes is copied from the router config and includes authorized routes.
    // That way, logged in users won't be thrown to the login screen if they refresh the page
    // while we determine with the server if they have the right credentials.

    var routes = angular.copy($route.routes);

    // We will need to switch where redirects get thrown to based on auth.
    // We could have a standard 404 page, but it seems useful
    // to redirect to the dashboard for authorized users and
    // the login page for non-authorized users.

    var secureRedirectPath = '/dashboard'; // app landing page
    var secureRedirect = angular.extend(angular.copy(routes['null']), {
        redirectTo: secureRedirectPath
    });
    var basicRedirect = angular.copy(routes['null']); // use default
    var basicRedirectPath = basicRedirect.redirectTo;

    var setBasicRoutes = function () {
        _.each($route.routes, function (config, path) {
            if (config.auth) {
                delete $route.routes[path];
                // if ($location.path() === path) {
                //     $location.path(basicRedirectPath);
                // }
            }
        });
        $route.routes['null'] = basicRedirect;
    };

    var setSecureRoutes = function () {
        _.each(routes, function (config, path) {
            var isUserAllowed = config.admin && session.userRole === 'admin' || !config.admin;
            if (isUserAllowed) {
                $route.routes[path] = config;
            } else {
                $route.routes[path] = secureRedirect;
                // TODO: Why do we have to do this here but not above?
                // How do we avoid seeing a flash when we go directly to the page?
                if ($location.path() === path) {
                    $location.path(secureRedirect);
                }
            }
        });
        $route.routes['null'] = secureRedirect;
    };

    /*
     * SESSION
     */

    // check for RSID from a prior session using a cookie
    var session = {
        rsid: $.cookie('lrs_rsid')
    };

    var isAuthenticated = function () {
        // SYNC
        // todo: is this the best way to do this?
        return session && session.userRole;
    };

    var setSession = function (newSession) {
        session = newSession;
        $.cookie('lrs_rsid', session.rsid);
        $rootScope.$broadcast('changeUser', session);
        console.log('session set', session);
        if (isAuthenticated) {
            setSecureRoutes();
        }
    };

    var clearSession = function () {
        session = {};
        $.removeCookie('lrs_rsid');
        $rootScope.$broadcast('changeUser', session);
        setBasicRoutes();
        console.log('session cleared');
    };

    var startSession = function () {
        console.log('startSession', session, session.rsid, !!session.rsid, API.GET());
        // if we have an RSID stored (from a cookie or after the login method),
        // attempt a request to get the session data
        return !!session.rsid ? // convert null to false
            RecruiterService.get({
                rsid: session.rsid
            }).then(function (response) {
                setSession(response.data);
                return response;
            }):
            $q.reject( clearSession() );
    };

    /*
     * PUBLIC METHODS
     */

    var service = {
        // getSession: function () {
        //     // ASYNC
        //     // attempt to return cached sessionUser
        //     return session ?
        //         $q.when({ data: session, cached: true }) : // wrap in promise object
        //         startSession()
        // },
        getUserRole: function () {
            // SYNC
            return session.userRole;
        },
        isAdmin: function () {
            // SYNC
            return this.getUserRole() === 'admin';
        },
        isAuthenticated: isAuthenticated,
        getSessionAsync: function () {
            // ASYNC
            // will cause route resolve to fail and redirect to login
            return this.isAuthenticated() ?
                $q.when(session):
                $q.when();
        },
        login: function (request) {
            var t = this;
            request = request || {};
            angular.extend(request, {
                method: 'POST',
                url: url,
                params: {
                    sinon: true
                }
            });
            return $http(request).then(function (response) {

                API.SET(response.data.token);
                setSession({rsid:response.data.rsid});

                console.log('login after request');

                // check
                return (response && response.data && response.data.token && response.data.rsid) ?
                    startSession().then(function (response) {
                        // update routes to allow access to authorized areas
                        // then redirect
                        setSecureRoutes();
                        console.log('redirecting from login after check');
                        $location.path(secureRedirectPath); // dashboard
                        return response;
                    }):
                    $q.reject( ); // clear session?
            });
        },
        logout: function (response) {
            console.log('logout');
            clearSession();
            API.CLEAR();
            if ($location.path() !== basicRedirectPath) { // login
                console.log('redirecting');
                $location.path(basicRedirectPath); // login
            }
            $rootScope.$broadcast('changeUser');
            return $http({
                method: 'DELETE',
                url: url,
                params: {
                    sinon: true
                }
            });
        },
        retrievePassword: function (request) {
            request = request || {};
            angular.extend(request, {
                method: 'POST',
                url: retrievePasswordUrl,
                params: {
                    sinon: true
                }
            });
            return $http(request).then(function (response) {
                response.data = 'Your password has been sent to the email provided.'; // TODO: here?
                return response;
            });
        },
        startRegister: function (request) {
            request = request || {};
            angular.extend(request, {
                method: 'POST',
                url: registerUrl,
                params: {
                    sinon: true
                }
            });
            return $http(request).then(function (response) {
                console.log('valid register');
                $location.path('/register'); //TODO test
            });
        }
    };

    /*
     * FORCE LOGOUT ON FAILURE
     */

     // 404 etc
    $rootScope.$on('failure', function () {
        service.logout();
    });

    // 403 specifically
    $rootScope.$on('unauthorized', function () {
        service.logout();
    });

    /*
     * INITIALIZE
     */

    // attempt to log in user using existing cookie credentials on page load
    startSession().then(function (response) {
        // on success
        // if user has come from the domain
        // the app will automatically leave them at login page
        // so redirect to dashboard
        console.log('SUCCESS FROM STARTSESSION', response);
        if ($location.path() === basicRedirectPath) { // login
            $location.path(secureRedirectPath); // dashboard
        }
    });

    return service;

});