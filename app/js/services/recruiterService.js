lrsServices.service('RecruiterService', function (API, $http, NotificationService) {
	var url = '/api/recruiter';
    var transformRecruiter = function (recruiter) {
        recruiter.name = recruiter.title + ' ' + recruiter.firstName + ' ' + recruiter.lastName;
        return recruiter;
    };

	return {
		get: function (request) {
            request = request || {};
            angular.extend(request, {
                method: 'GET',
                url: url + '/' + request.rsid,
                params: {
                    sinon: true
                }
            });
			return $http(request).then(function (response) {
                transformRecruiter(response.data);
                return response;
            });
		},
        query: function (request) {
            request = request || {};
            angular.extend(request, {
                method: 'GET',
                url: url,
                params: {
                    sinon: true
                }
            });
            return $http(request);
        }
	};
});