lrsServices.service('NotificationService', function ($rootScope) {
	return {
		reportSuccess: function (response) {
			// TODO google analytics push
			var successMessage = angular.isFunction(response.config.successMessage) ?
				response.config.successMessage(response) : response.config.successMessage;
			$rootScope.$broadcast('success', successMessage);
			return response;
		},
		reportUnauthorized: function (response) {
			$rootScope.$broadcast('unauthorized', {
				error: response
			});
			return response;
		},
		reportFailure: function (response) {
			// TODO google analytics push
			// console.log('failure!', response);
			$rootScope.$broadcast('failure', {
                error: response
			});
			return response;
		}
	};
});