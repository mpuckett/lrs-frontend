lrsServices.service('ContactService', function (API, $http, $rootScope, NotificationService, AppointmentService, $q, SessionService) {

	var contactUrl = '/api/contact';
	var recruiterUrl = '/api/recruiter';
	var candidateUrl = '/api/candidate';

	return {
		add: function (data) {
			var deferred = $q.defer;
			$rootScope.$broadcast('alert', {
                partial: '/partials/contacts/add.html',
                data: data,
                deferred: deferred
            });
            return deferred.promise;
		},
		getRecruiterContactHistory: function (request) {
			request = request || {}; // TODO: Bring this back!
            angular.extend(request, {
                method: 'GET',
                url: recruiterUrl + '/' + ( request.rsid || API.RSID ) + '/contact-history',
                params: {
                    sinon: true
                }
            });
            return $http(request);
        },
        getCandidateContactHistory: function (request) {
        	request = request || {};
        	angular.extend(request, {
        		method: 'GET',
        		url: candidateUrl + '/' + request.personID + '/contact-history',
        		params: {
        			sinon: true
        		}
        	});
        	return $http(request);
        },
		create: function (request) {
			
			// TODO: wrong way to determine if has appointment
			// TODO Update UI - Appointment is not a type of contact
			// var hasAppointment = request.data.contact.type === 'appointment';
			// var appointmentData = angular.extend(angular.copy(request.data.appointment), {
			// 	notes: request.data.notes
			// });

			request = request || {};
			// TODO: This will include all the other appointment data too
			// TODO: Get this out of this service anyway
			request = angular.extend(request, {
				method: 'POST',
				url: contactUrl,
				params: {
					sinon: true
				},
				successMessage: {
					type: 'Add Contact',
					title: 'Contact Added',
					description: 'Congratulations! You successfully added a contact.'
				}
			});

			//successMessage: {
				// 	type: 'Add Contact',
				// 	title: 'Contact Added',
				// 	description: hasAppointment ?
				// 		'Congratulations! You successfully added a contact and an appointment.' :
				// 		'Congratulations! You successfully added a contact.'
				// }

			return $http(request).then(function (response) {
				(!request.preventDefault && NotificationService.reportSuccess || angular.noop)(response);
				return response;
			});
		},
		query: function (opts) {
			request = request || {};
			angular.extend(request, {
				method: 'GET',
				url: contactUrl + '/' + request.personID,
				params: {
					sinon: true
				}
			});
			return $http(request);
		}
	}
});