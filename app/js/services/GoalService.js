lrsServices.service('GoalService', function (API, $http, $cacheFactory, NotificationService) {
	var url = '/api/recruiter/' + API.RECRUITER_ID + '/goal';

	// TODO: Cache

	return {
		query: function (request) {
			request = request || {};
			angular.extend(request, {
				method: 'GET',
				url: url,
				params: {
					sinon: true
				}
			});
			return $http(request).then(function (response) {
				angular.forEach(response.data, function (goal) {
					// cache.put(goal);
				});
				return response;
			});
		},
		update: function (request) {
			request = request || {};
			angular.extend(request, {
				method: 'PUT',
				url: url,
				params: {
					sinon: true
				},
				successMessage: {
					type: 'Update Goals',
					title: 'Mission Goals Updated',
					description: 'Congratulations! You successfully updated your Mission Goals.' // TODO: Month or Week?
				}
			});
			return $http(request).then(function (response) {
				(!request.preventDefault && NotificationService.reportSuccess || angular.noop)(response);
				return response;
			});
		},
		send: function (request) {
			request = request || {};
			angular.extend(request, {
				method: 'POST',
				url: url,
				params: {
					sinon: true
				},
				successMessage: {
					type: 'Send to NCOIC',
					title: 'Missions Goals Sent to NCOIC',
					description: 'Congratulations! You successfully sent your Mission Goals to your NCOIC.'
				}
			});
			return $http(request).then(function (response) {
				(!request.preventDefault && NotificationService.reportSuccess || angular.noop)(response);
				return response;
			});
		}
	};
});