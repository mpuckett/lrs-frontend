lrsServices.service('CandidateService', function ($q, $http, $cacheFactory, $rootScope, NotificationService) {

    var url = '/api/candidate',
        importUploadFileUrl = '/api/upload-file',
        importUploadUrl = '/api/upload',
        importMapUrl = '/api/map',
        importValidateUrl = '/api/validate',
        importProcessUrl = '/api/process',
        cache = $cacheFactory('candidates');

    var transformCandidate = function (candidate) {
        candidate.firstName = 'Bob'; // TODO hahaha
        cache.put(candidate.personID, candidate);
        return candidate;
    };
    // NEED THE REVERSE OF THIS TO REBUILD FOR SERVER
    // do i need to? do i need to remove the stuff i add?
    // turn into a service?

    var getPagesArray = function (totalPages) {
        var pages = [];
        for (var i=0; i < totalPages; i++) {
            pages.push(i+1);
        }
        return pages;
    };

    var getPaginationByRangeHeader = function (response) {

        // parse content-range header for pagination
        // TODO - changed config to params

        var headers = response.headers()['content-range'].split('/');
        var totalRecords = parseInt(headers[1], 10);
        var range = headers[0].split('-');
        var start = parseInt(range[0], 10) + 1; // 1
        var stop = parseInt(range[1], 10) + 1; // 25
        var limit = response.config.params.limit;
        var totalPages = (totalRecords / limit);
        var currentPage = Math.min( Math.floor(start / limit) + 1, totalPages); // needs upper limit
        var pages = getPagesArray(totalPages);
        var isLast = currentPage === totalPages;
        var isFirst = currentPage === 1;

        return {
            currentPage: currentPage,
            start: start,
            stop: stop,
            totalRecords: totalRecords,
            totalPages: totalPages,
            pages: pages,
            isFirst: isFirst,
            isLast: isLast
        };
    };

    var getRangeHeaderByPagination = function (params) {
        params = params || {};
        var page = params.page && params.page - 1 || 0;
        var limit = params.limit || 25;

        var start = (page * limit);
        var stop = start + limit - 1;
        var range = start + '-' + stop;

        return range;
    };

    var uploadFileWithIframe = function (form) {
        var deferred = $q.defer();
        var $form = $(form);
        var url = importUploadFileUrl;
        var timeout = 25*1000; // reject after 25 seconds
        var iframeName = 'uploadFileIframe';
        var $iframe = $('<iframe id="' + iframeName + '" src="' + url + '" style="display:none" />');
        $('body').append($iframe);
        $iframe.on('load', function (e) {
            clearTimeout(timer);
            var response = {
                data: $.parseJSON($iframe.contents().find('body').html())
            };
            $iframe.remove();
            $form.removeAttr('target').removeAttr('method').removeAttr('enctype');
            $rootScope.$apply(function() {
                deferred.resolve(response);
            });
        });
        var timer = setTimeout(function () {
            $rootScope.$apply(function() {
                deferred.reject();
            });
        }, timeout);
        $form
            .attr('target', iframeName)
            .attr('method', 'POST')
            .attr('enctype', 'multipart/form-data')
            .submit();
        return deferred.promise;
    };

    return {
        candidateTypes: {
            'lead': {
                'name': {
                    'singular': 'Lead',
                    'plural': 'Leads'
                },
                'vilpacStatus': 'lead'
            },
            'processing': {
                'name': {
                    'singular': 'Candidate in Process',
                    'plural': 'Candidates in Process'
                },
                'vilpacStatus': 'processing'
            }
        },
        showContactDetails: function (data) {
            var deferred = $q.defer();
            $rootScope.$broadcast('alert', {
                partial: '/partials/contacts/candidate-details.html',
                data: data,
                deferred: deferred
            });
            return deferred.promise;
        },
        query: function (request) {
            angular.extend(request || {}, {
                method: 'GET',
                url: url,
                headers: {
                    'Range': getRangeHeaderByPagination(request.params)
                },
                params: angular.extend(request.params || {}, {
                    sinon: true
                })
            });
            return $http(request).then(function (response) {
                angular.forEach(response.data, transformCandidate);
                angular.extend(response, {
                    paging: getPaginationByRangeHeader(response)
                });
                return response;
            });
        },
        get: function (request) {
            // request = request || { params: {} };
            // TODO - does angular.extend(undefined || {}, {}) work or not?
            angular.extend(request || {}, {
                method: 'GET',
                url: url+'/'+request.personID,
                cache: cache,
                params: angular.extend(request.params || {}, {
                    sinon: true
                })
            });

            // attempt to return from the local cache first
            var cached = cache.get(request.personID);

            return cached ?
                $q.when({data: cached, cached: true}) : // wrap in a promise object
                $http(request).then(function (response) {
                    transformCandidate(response.data);
                    return response;
                });
        },
        create: function (request) {
            request = request || {};
            angular.extend(request, {
                method: 'POST',
                url: url,
                params: {
                    sinon: true
                },
                successMessage: {
                    type: 'Add Lead',
                    title: 'You have added 1 Lead',
                    description: 'Congratulations, you have added ' + request.data.firstName + ' ' + request.data.lastName + ' as a new lead.'
                }
            });
            return $http(request).then(function (response) {
                angular.extend(response.data, request.data);
                transformCandidate(response.data);
                (!request.preventDefault && NotificationService.reportSuccess || angular.noop)(response);
                return response;
            });
        },
        importUpload: function (request) {
            // expecting file (request.form) to upload and import options

            // first upload file using old-school iframe hack
            request = request || {};
            var form = request.form;
            return uploadFileWithIframe(request.form)
                .then(function(response) {
                    console.log(request, response);
                    // no longer needed after iframe hack
                    delete request.form;
                    // add resulting file ID to original request
                    angular.extend(request.data, response.data);
                    // then modify record with import options
                    angular.extend(request, {
                        method: 'PUT',
                        url: importUploadUrl + '/' + request.data.id,
                        params: {
                            sinon: true
                        }
                    });
                    return $http(request).then(function (response) {
                        // expecting record with columns
                        angular.extend(response.data, response.config.data);
                        return response;
                    });
                });
        },
        importMap: function (request) {
            // expecting record with mapped columns
            request = request || {};
            angular.extend(request, {
                method: 'PUT',
                url: importUploadUrl + '/' + request.data.id,
                params: {
                    sinon: true
                }
            });
            return $http(request).then(function (response) {
                // expecting success notification
                angular.extend(response.data, response.config.data);
                return response;
            });
        },
        importValidate: function (request) {
            // expecting record (with mapped columns)
            console.log('import validate', request);
            request = request || {};
            angular.extend(request, {
                method: 'POST',
                url: importValidateUrl + '/' + request.data.id,
                params: {
                    sinon: true
                }
            });
            return $http(request).then(function (response) {
                // expecting record with validation data
                angular.extend(response.data, response.config.data);
                return response;
            });
        },
        importProcess: function (request) {
            // expecting record (with mapped columns and validation data)
            console.log('import process', request);
            request = request || {};
            angular.extend(request, {
                method: 'POST',
                url: importProcessUrl + '/' + request.data.id,
                params: {
                    sinon: true
                },
                successMessage: function (response) {
                    return {
                        type: 'Import Leads',
                        title: 'You have imported ' + response.data.total + ' Leads',
                        description: 'Congratulations, you have imported ' + response.data.total + ' new leads. The NCOIC and the RRNCO(s) who received a lead will receive an email notification regarding the imported leads.'
                    };
                }
            });
            return $http(request).then(function (response) {
                // expecting record with processing data
                angular.extend(response.data, response.config.data);
                // do we need the actual leads? do we want to store them in cache? TODO
                (!request.preventDefault && NotificationService.reportSuccess || angular.noop)(response);
                return response;
            });
        }
    };
});