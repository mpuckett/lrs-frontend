lrsServices.service('SavedSearchService', function(API, $q, $http, $cacheFactory, $rootScope, SessionService, NotificationService) {

	var url = '/api/recruiter/' + API.RECRUITER_ID + '/saved-search';
        cache = $cacheFactory('savedSearches');

	return {
        query: function (request) {
            request = request || { params: {} }
            angular.extend(request, {
                method: 'GET',
                url: url,
                params: angular.extend(request.params, {
                    sinon: true
                }),
                cache: cache
            });
            return $http(request);
        },
        add: function (search, data) {
            // prompt user to name the saved search
            var deferred = $q.defer();
            data = data || {};
            angular.extend(data, {
                search: search
            });
            $rootScope.$broadcast('alert', {
                partial: '/partials/saved-searches/add.html',
                data: data,
                deferred: deferred
            });
            return deferred.promise;
        },
        rename: function (request) {
            request = request || {};
            request.params = request.params || {};
            angular.extend(request, {
                method: 'PUT',
                url: url + '/' + request.id,
                params: angular.extend(request.params, {
                    sinon: true
                }),
                successMessage: {
                    type: 'Update Saved Search',
                    title: 'You have updated 1 Saved Search',
                    description: 'Congratulations, you have renamed a Saved Search to ' + request.data.name + '.'
                }
            });
            return $http(request).then(function (response) {
                
                (!request.preventDefault && NotificationService.reportSuccess || angular.noop)(response);
                return response;
            });
        },
        remove: function (request) {
            request = request || {};
            request.params = request.params || {};
            angular.extend(request, {
                method: 'DELETE',
                url: url + '/' + request.data.id,
                params: angular.extend(request.params, {
                    sinon: true
                }),
                successMessage: {
                    type: 'Update Deleted 1 Saved Search',
                    title: 'You have Deleted 1 Saved Search',
                    description: 'Congratulations, you have Deleted 1 Saved Search.'
                }
            });
            return $http(request).then(function (response) {
                
                (!request.preventDefault && NotificationService.reportSuccess || angular.noop)(response);
                console.log('request', request);
                return response;

            });
        },
        create: function (request) {
            // add the saved search once it has been named
            angular.extend(request || {}, {
                method: 'POST',
                url: url,
                params: {
                    sinon: true
                },
                // don't need to transform because we never GET (we query all instead)
                // transform: function (response) {
                //     cache.put(response.id, request.data);
                //     return response;
                // },
                successMessage: {
                    type: 'Saved Search',
                    title: 'You have added a new Saved Search',
                    description: 'Congratulations, you have added ' + request.data.name + ' as a new Saved Search.'
                }
            });
            return $http(request).then(function(response) {
                (!request.preventDefault && NotificationService.reportSuccess || angular.noop)(response);
                return response;
            });
        }
    };
});