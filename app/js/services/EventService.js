lrsServices.service('EventService', function ($q, $http, $cacheFactory, $rootScope, NotificationService) {
	
	var url = '/api/event',
		cache = $cacheFactory('events');

	return {
		query: function (request) {
			request = request || {};
			angular.extend(request, {
				method: 'GET',
				url: url,
				cache: cache,
				params: {
					sinon: true
				},
				transform: function (events) {
					angular.forEach(events, function (event) {
						cache.put(event.id, event);
					});
					return events;
				}
			});
			return $http(request);
		},
		add: function (request) { // TODO Success & Failure?
			var deferred = $q.defer();
			$rootScope.$broadcast('alert', {
				partial: '/partials/events/add.html',
				deferred: deferred
			});
			return deferred.promise;
		},
		create: function (request) {
			request = request || {};

			console.log(request.data.time);

			angular.extend(request, {
				method: 'POST',
				url: url,
				params: {
					sinon: true
				},
				data: angular.extend(request.data),
				successMessage: {
	                type: 'Event',
	                title: 'You have added a new Event',
	                description: 'Congratulations, you have added ' + request.data.title + ' as a new Event.'
	            },
				transform: function (response) {
					cache.put(response.id, angular.extend(request.data, response.id));
					return response;
				}
			});
			return $http(request).then(function (response) {
				angular.extend(response.data, request.data);
				cache.put(response.data.id, response.data);
				(!request.preventDefault && NotificationService.reportSuccess || angular.noop)(response);
				return response;
			});
		}
	}
});