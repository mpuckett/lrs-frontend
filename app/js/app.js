// TODO:
// Validation needs little arrows everywhere like in PSD
// Modals need stylin'

// Declare app level module which depends on filters, and services
// TODO: Check to see if I need to include ui.directives here?
// TODO: CookieProvider? SantizeProvder?
var lrs = angular.module('lrs', ['ui','lrsFilters','lrsServices','lrsDirectives']).
config(['$routeProvider', function($routeProvider) {

    // public routes

    $routeProvider.
    when('/login', {
        templateUrl: 'partials/auth/login.html',
        controller: LoginCtrl
    }).
    when('/support', {
        templateUrl: 'partials/support/index.html'
    }).
    when('/forgot-password', {
        templateUrl: 'partials/auth/forgot-password.html',
        controller: ForgotPasswordCtrl
    }).
    when('/reset-password', {
        templateUrl: 'partials/auth/reset-password.html',
        controller: ResetPasswordCtrl
    }).
    when('/register', {
        templateUrl: 'partials/auth/register.html'
    }).
    otherwise({
        redirectTo: '/login'
    });

    var secureRoutes = {
        '/dashboard': {
            templateUrl: 'partials/dashboard/index.html',
            controller: DashboardCtrl,
            resolve: {
                Goals: function (GoalService) {
                    return GoalService.query().then(function (response) {
                        return response.data;
                    });
                },
                Qualified: function (CandidateService) {
                    return CandidateService.query({params:{status:'qualified'}}).then(function (response) {
                        return response.data;
                    });
                },
                Unscreened: function (CandidateService) {
                    return CandidateService.query({params:{status:'unscreened'}}).then(function (response) {
                        return response.data;
                    });
                },
                GoGuard: function (CandidateService) {
                    return CandidateService.query({params:{status:'goGuard'}}).then(function (response) {
                        return response.data;
                    });
                },
                Generated: function (CandidateService) {
                    return CandidateService.query({params:{status:'generated'}}).then(function (response) {
                        return response.data;
                    });
                },
                Contracts: function (CandidateService) {
                    return CandidateService.query({params:{vilpacStatus:'contract'}}).then(function (response) {
                        return response.data;
                    });
                },
                Appointments: function (AppointmentService) {
                    return AppointmentService.query().then(function (response) {
                        return response.data;
                    });
                },
                Events: function (EventService) {
                    return EventService.query().then(function (response) {
                        return response.data;
                    });
                },
                Contacts: function (ContactService) {
                    return ContactService.getRecruiterContactHistory().then(function (response) {
                        return response.data;
                    });
                }
            }
        },
        '/goals': {
            templateUrl: 'partials/goals/index.html',
            controller: GoalsCtrl,
            resolve: {
                Goals: function (GoalService) {
                    return GoalService.query().then(function (response) {
                        return response.data;
                    });
                }
            }
        },
        '/leads': {
            templateUrl: 'partials/candidates/search/index.html',
            controller: SearchCandidatesCtrl,
            resolve: {
                SavedSearches: function (SavedSearchService) {
                    return SavedSearchService.query({params:{vilpacStatus: 'lead'}}).then(function (response) {
                        console.log('y u no unshift?', response);
                        // response.data.unshift({ 'name': 'Select a Search' }); // TODO: What is this
                        return response.data;
                    });
                },
                View: function () { return 'list'; },
                CandidateType: function () { return 'lead'; }
            }
        },
        '/processing': {
            templateUrl: 'partials/candidates/search/index.html',
            controller: SearchCandidatesCtrl,
            resolve: {
                SavedSearches: function (SavedSearchService) {
                    return SavedSearchService.query({params:{vilpacStatus: 'processing'}}).then(function (response) {
                        console.log('y u no unshift?', response);
                        // response.data.unshift({'name':'Select a Search' }); // TODO: What is this
                        return response.data;
                    });
                },
                View: function () { return 'list'; },
                CandidateType: function () { return 'processing'; }
            }
        },
        '/leads/add': {
            templateUrl: 'partials/candidates/manage/index.html',
            controller: AddCandidateCtrl,
            resolve: {
                CandidateType: function () { return 'lead'; }
            }
        },
        '/leads/edit/:personID': {
            templateUrl: 'partials/candidates/manage/index.html',
            controller: EditCandidateCtrl,
            resolve: {
                Candidate: function (CandidateService, $routeParams) {
                    return CandidateService.get({ personID: $routeParams.personID }).then(function (response) {
                        return response.data;
                    });
                },
                CandidateType: function () { return 'lead'; }
            }
        },
        '/processing/edit/:personID': {
            templateUrl: 'partials/candidates/manage/index.html',
            controller: EditCandidateCtrl,
            resolve: {
                Candidate: function (CandidateService, $routeParams) {
                    return CandidateService.get({ personID: $routeParams.personID }).then(function (response) {
                        return response.data;
                    });
                },
                CandidateType: function () { return 'processing'; }
            }
        },
        '/profile': {
            templateUrl: 'partials/profile/index.html',
            controller: EditProfileCtrl, // TODO empty resolve OK?
            resolve: {
                User: function (SessionService) {
                    return SessionService.getSessionAsync();
                }
            }
        }
    };

    var adminRoutes = {
        '/leads/import': {
            templateUrl: 'partials/candidates/import/index.html',
            controller: ImportCandidatesCtrl
        }
    };

    angular.forEach(secureRoutes, function (config, path) {
        // var resolve = config.resolve || {};
        // for checking if user is logged in and not showing them a flash of page
        // resolve['User'] = function (SessionService) {
        //     return SessionService.getSessionAsync();
        // };
        angular.extend(config, {
            auth: true//, for updating routes in SessionService
            // resolve: resolve
        });
        $routeProvider.when(path, config);
    });

    angular.forEach(adminRoutes, function (config, path) {
        angular.extend(config, {
            auth: true,
            admin: true
        });
        $routeProvider.when(path, config);
    });

}]);

lrs.constant('API',
    // self-executing function
    (function () {
        var COOKIE = 'lrs_auth_token';
        var HEADERS; // direct access to all headers
        var AUTH_HEADER; // name of auth header
        var TOKEN = $.cookie(COOKIE);
        var apiConfig = {
            common: {
                INIT: function (headers, authHeader) {
                    HEADERS = headers;
                    AUTH_HEADER = authHeader;
                    HEADERS[AUTH_HEADER] = TOKEN;
                },
                GET: function () {
                    return TOKEN;
                },
                SET: function (token) {
                    TOKEN = token;
                    $.cookie(COOKIE, TOKEN); // set cookie
                    HEADERS[AUTH_HEADER] = TOKEN; // update headers
                },
                CLEAR: function () {
                    TOKEN = null;
                    $.removeCookie(COOKIE);
                    HEADERS[AUTH_HEADER] = TOKEN; // update headers
                }
            },
            dev: {
                URL: 'http://www.cognac.dev/api/'
            },
            prod: {
                URL: ''
            }
        };
        return angular.extend(apiConfig.common, apiConfig.dev);
    }())
);

/* Init Services */

var lrsServices = angular.module('lrsServices', []);

/* Init Directives */

var lrsDirectives = angular.module('lrsDirectives', []);

lrs.run(function($rootScope, $location, SessionService) {

});
// moved ui.config value to values()