lrsDirectives
.directive('placeholder', function($timeout){
    // TODO this will be depricated soon
    if (!$.browser.msie || $.browser.version >= 10) {
        return {};
    }
    return {
        link: function(scope, elm, attrs){
            if (attrs.type === 'password') {
                return;
            }
            $timeout(function(){
                elm.val(attrs.placeholder).focus(function(){
                    if ($(this).val() == $(this).attr('placeholder')) {
                        $(this).val('');
                    }
                }).blur(function(){
                    if ($(this).val() === '') {
                        $(this).val($(this).attr('placeholder'));
                    }
                });
            });
        }
    };
})
.directive('ioChange', function(){
    return {
        link: function($scope, el, attrs){
            var $el = $(el);
            $el.on('change', function () {
                $scope[attrs['ioChange']]( $el.val() );
            });
        }
    };
})
.directive('fileUpload', function () {


    var linker = function ($scope, el, attrs) {
        opts = $.parseJSON(attrs['fileUpload']) || {};
        console.log( opts );

         $(el).fileupload({
            dataType: 'json',
            done: attrs
        });
    };

    return {
        restrict: 'A',
        link: linker
    };
})
.directive('chosenList', function() {
    // angular.element(document).ready(function() {
        var linker = function($scope, element, attr) {

            element.chosen({allow_single_deselect: true});

            // enable two-way data-binding!
            $scope.$watch(attr.ngModel, function () {
                element.trigger('liszt:updated');
            }, true);

            // enable two-way data-binding!
            $(element).on('change', function () {
                element.trigger('liszt:updated');
            });

        };

        return {
            restrict: 'A', // attribute
            terminal : true, // last to be executed
            require: 'ngModel',
            link: linker
        }
    // });
})
.directive('ioDateTrigger', function() {
    var linker = function ($scope, element, attr) {
        $(element).on('click', function (e) {
            e.preventDefault();
            // var $el = $(this).siblings('.hasDatepicker'),
            var $el = $(this).closest('label').find('.hasDatepicker');
                visible = $el.datepicker('widget').is(':hidden');
            $el.datepicker( visible ? 'show' : 'hide' );
        });
    };
    return {
        restrict: 'A',
        link: linker,
        terminal: true
    }
})
.directive('ioDatetime', function () {
    var linker = function ($scope, element, attr) {
        $(element).datetimepicker('setDate', $scope[attr.ioDatetime]);
    };
    return {
        restrict: 'A',
        link: linker
    };
})
.directive('ioVisible', function () {
    var linker = function ($scope, element, attr) {
        var expression = attr.ioVisible;
        $scope.$watch(expression, function (newValue) {
            var visibility = 'hidden';
            if (newValue) {
                visibility = 'visible';
            }
            $(element).css({'visibility':visibility});
        });

    };
    return {
        restrict: 'A',
        link: linker
    };
})
.directive('ioSpinner', function () {
    var opts = {
        lines: 13,
        length: 5,
        width: 2,
        radius: 7
    };
    var linker = function ($scope, element, attr) {
        var expression = attr.ioSpinner;
        opts = expression ?
            angular.extend(opts, { 'className': expression }):
            opts;
        console.log(expression, attr, opts);
        var spinner = new Spinner(opts).spin(element[0]);
    };

    return {
        restrict: 'A',
        link: linker
    };
})
.directive('ioUniqueIn', function() { // used for Add Lead Form validation
    return {
        priority: -1,
        require: 'ngModel',
        link: function(scope, el, attr, ctrl) {

            var set = scope[attr.ioUniqueIn],
                id = $(el).attr('id'),
                validator = function (value) {
                    // if (set) {
                        set[id] = value;
                    // }
                    return value;
                };
            
            ctrl.$formatters.push(validator);
            ctrl.$parsers.push(validator);

            scope.$watch(
                // TODO: Runs every time anything in the entire model changes!
                function () {
                    var hasLength = _.chain(set).toArray().compact().value().length > 0; // TODO: Expensive?
                    // var hasLengthObj = _.isEmpty(set); // Doesn't work - doesn't check for empty values
                    // console.log('watching', hasLength, scope._unique);
                    ctrl.$setValidity('unique', hasLength );
                    // ctrl.$setValidity('unique', true );
                }
            );

        }
    };
})
.directive('showToolbox', function () {
    var linker = function ($scope, element, attr) {
        //console.log($scope);
        $scope.$watch('shown', function(t) {

            if(t == true)
                $('body').addClass('show-toolbox');
            else
                $('body').removeClass('show-toolbox');

        });
    };
    return {
        restrict: 'A',
        link: linker
    }
});



