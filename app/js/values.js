// TODO: appointments to appointmentTypes
lrs.value('Options', {
    appointments: [
        { label: 'Appointment Type' },
        { label: 'Face to Face', value: 'Face to Face' },
        { label: 'MEPS', value: 'MEPS' },
        { label: 'ASVAB', value: 'ASVAB' }
    ],
    booleans: [
        { label: 'Choose One' },
        { label: 'No', value: 0 },
        { label: 'Yes', value: 1 }
    ],
    candidateSortFields: [
        { label: 'Last Name (Ascending)', value: 'lastName' },
        { label: 'Last Name (Descending)', value: '-lastName' },
        { label: 'Age (Ascending)', value: 'age' },
        { label: 'Age (Descending)', value: '-age' },
        { label: 'City (Ascending)', value: 'city' },
        { label: 'City (Descending)', value: '-city' },
        { label: 'State (Ascending)', value: 'state' },
        { label: 'State (Descending)', value: '-state' },
        { label: 'Registered Date (Ascending)', value: 'registered' },
        { label: 'Registered Date (Descending)', value: '-registered' },
        { label: 'Last Action Date (Ascending)', value: 'lastAction' },
        { label: 'Last Action Date (Descending)', value: '-lastAction' },
        { label: 'Appointment Date (Ascending)', value: 'appointment' },
        { label: 'Appointment Date (Descending)', value: '-appointment' },
        { label: 'Packet Completion (Ascending)', value: 'packet' },
        { label: 'Packet Completion (Descending)', value: '-packet' }
    ],
    contactTypes: [
        { label: 'Left Message', value: 'message' },
        { label: 'Spoke', value: 'spoke' },
        { label: 'Made Appointment', value: 'appointment' }
    ],
    dispositions: [
        { label: 'Disposition' },
        { label: 'Convert to Applicant', value: 22 },
        { label: 'Record Review', value: 14 },
        { label: 'Follow-Up', value: 17 },
        { label: 'Appointment Made', value: 18 },
        { label: 'Appointment Conducted', value: 19 },
        { label: 'Schedule Callback', value: 21 },
        { label: 'Not Interested - Continuing Education', value: 2 },
        { label: 'Not Interested - Follow Up', value: 3 },
        { label: 'Not Interested - Joined Other Service', value: 4 },
        { label: 'Not Interested - College', value: 1 },
        { label: 'Temporarily Disqualified - Age', value: 5 },
        { label: 'Temporarily Disqualified - Physical (HT/WT)', value: 6 },
        { label: 'Temporarily Disqualified - Legal', value: 7 },
        { label: 'Temporarily Disqualified - Education', value: 8 },
        { label: 'Temporarily Disqualified - Marital Status', value: 9 },
        { label: 'Temporarily Disqualified - Dependency', value: 10 },
        { label: 'Permanently Disqualified - Age', value: 11 },
        { label: 'Permanently Disqualified - Physical/Medical', value: 12 },
        { label: 'Permanently Disqualified - Moral/Legal', value: 13 },
        { label: 'Referred - AMEDD', value: 24 },
        { label: 'Referred - Chaplain', value: 25 },
        { label: 'Referred - OSM', value: 26 },
        { label: 'Referred - SF', value: 27 },
        { label: 'Referred - WOSM', value: 28 },
        { label: 'Referred - Other Service', value: 29 },
        { label: 'Referred - Air National Guard', value: 30 },
        { label: 'Referred - RRNCO', value: 23 }
    ],
    events: [
        { label: 'Event Type' },
        { label: 'Face to face', value: 1 },
        { label: 'Office visit', value: 2 },
        { label: 'Telephone call', value: 3 },
        { label: 'Unit visit', value: 4 },
        { label: 'High school visit', value: 5 },
        { label: 'College visit', value: 6 },
        { label: 'Area canvas', value: 7 },
        { label: 'RPI response', value: 8 },
        { label: 'Other', value: 9 }
    ],
    genders: [
        { label: 'Gender' },
        { label: 'Female', value: 'Female' },
        { label: 'Male', value: 'Male' }
    ],
    grades: [
        { label: 'Highest Grade' },
        { label: '10th Grade', value: 10 },
        { label: '11th Grade', value: 11 },
        { label: 'High School Graduate', value: 12 },
        { label: '1 year of College', value: 13 },
        { label: '2 years of College / Associates Degree', value: 14 },
        { label: '3 years of College', value: 15 },
        { label: 'Bachelor\'s Degree', value: 16 },
        { label: 'Post Graduate Studies', value: 17 }
    ],
    states: [
        { label: 'State' },
        { label: 'Alabama', value: 'AL' },
        { label: 'Alaska', value: 'AK' },
        { label: 'American Samoa', value: 'AS' },
        { label: 'Arizona', value: 'AZ' },
        { label: 'Arkansas', value: 'AR' },
        { label: 'California', value: 'CA' },
        { label: 'Colorado', value: 'CO' },
        { label: 'Connecticut', value: 'CT' },
        { label: 'Delaware', value: 'DE' },
        { label: 'District of Columbia', value: 'DC' },
        { label: 'Florida', value: 'FL' },
        { label: 'Georgia', value: 'GA' },
        { label: 'Guam', value: 'GU' },
        { label: 'Hawaii', value: 'HI' },
        { label: 'Idaho', value: 'ID' },
        { label: 'Illinois', value: 'IL' },
        { label: 'Indiana', value: 'IN' },
        { label: 'Iowa', value: 'IA' },
        { label: 'Kansas', value: 'KS' },
        { label: 'Kentucky', value: 'KY' },
        { label: 'Louisiana', value: 'LA' },
        { label: 'Maine', value: 'ME' },
        { label: 'Marshall Islands', value: 'MH' },
        { label: 'Maryland', value: 'MD' },
        { label: 'Massachusetts', value: 'MA' },
        { label: 'Michigan', value: 'MI' },
        { label: 'Minnesota', value: 'MN' },
        { label: 'Mississippi', value: 'MS' },
        { label: 'Missouri', value: 'MO' },
        { label: 'Montana', value: 'MT' },
        { label: 'Nebraska', value: 'NE' },
        { label: 'Nevada', value: 'NV' },
        { label: 'New Hampshire', value: 'NH' },
        { label: 'New Jersey', value: 'NJ' },
        { label: 'New Mexico', value: 'NM' },
        { label: 'New York', value: 'NY' },
        { label: 'North Carolina', value: 'NC' },
        { label: 'North Dakota', value: 'ND' },
        { label: 'Northern Mariana Islands', value: 'MP' },
        { label: 'Ohio', value: 'OH' },
        { label: 'Oklahoma', value: 'OK' },
        { label: 'Oregon', value: 'OR' },
        { label: 'Palau', value: 'PW' },
        { label: 'Pennsylvania', value: 'PA' },
        { label: 'Puerto Rico', value: 'PR' },
        { label: 'Rhode Island', value: 'RI' },
        { label: 'South Carolina', value: 'SC' },
        { label: 'South Dakota', value: 'SD' },
        { label: 'Tennessee', value: 'TN' },
        { label: 'Texas', value: 'TX' },
        { label: 'Utah', value: 'UT' },
        { label: 'Vermont', value: 'VT' },
        { label: 'Virgin Islands', value: 'VI' },
        { label: 'Virginia', value: 'VA' },
        { label: 'Washington', value: 'WA' },
        { label: 'West Virginia', value: 'WV' },
        { label: 'Wisconsin', value: 'WI' },
        { label: 'Wyoming', value: 'WY' }
    ]    
});