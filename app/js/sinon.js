var server = sinon.fakeServer.create();

server.autoRespond = true;
server.autoRespondAfter = 600;

sinon.fakeServer.xhr.useFilters = true;
sinon.fakeServer.xhr.addFilter(function (method, url) {
    var params = URI(url).search(true);
    var shouldIntercept = params.sinon;
    if (shouldIntercept) {
        console.log('Sinon finished loading: "' + URI(url).search('') + '" using ' + method + '.');
        delete params.sinon;
        if ( !_.isEmpty(params) ) {
            console.log('With params: ', params);
        }
    }
    // var shouldIntercept = url.indexOf('sinon') > -1;
    return !shouldIntercept;
});

server.check = function (args) {
    var xhr = args[0];
    var token = xhr.requestHeaders['Auth-Token'];
    xhr.requestBody && console.log($.parseJSON(xhr.requestBody));
    if (token && token.length > 0) {
        return true;
    } else {
        console.log(403, xhr.url, token);
        xhr.respond(403);
        return false;
    }
};

/* FORGOT PASSWORD */
// post
// /api/retrievePassword
server.respondWith(
    "POST",
    /\/api\/retrieve-password/,
    function (xhr, rsid) {
        // var args = arguments; server.check(args);
        console.log('responding');
        xhr.respond(
            200,
            { "Content-Type": "application/json" },
            '{}'
        );
    }
);

/* REGISTER */
// post
// /api/register
server.respondWith(
    "POST",
    /\/api\/register/,
    function (xhr) {
        var data = $.parseJSON(xhr.requestBody);
        if (data && data.ssn && data.ssn == 1234) {
            xhr.respond(
                200,
                { "Content-Type": "application/json" },
                '{}'
            )
        } else {
            xhr.respond(
                403,
                { "Content-Type": "application/json" },
                '{}'
            )
        }
    }
);

/* UPLOAD */
// post
// /api/upload
server.respondWith(
    "PUT",
    /\/api\/map/,
    function (xhr) {
        var args = arguments; server.check(args);
        xhr.respond(
            200,
            { "Content-Type": "application/json" },
            '{"columns":[\
                "fName",\
                "lName",\
                "Email",\
                "SSN",\
                "Phone",\
                "Address",\
                "City",\
                "State",\
                "ZIP",\
                "Preferred Contact Method",\
                "Highest Grade",\
                "Gender",\
                "Prior Service",\
                "Date Contacted",\
                "Notes"\
            ]}'
        );
    }
);

/* UPLOAD */
// post
// /api/upload
server.respondWith(
    "PUT",
    /\/api\/upload/,
    function (xhr) {
        var args = arguments; server.check(args);
        xhr.respond(
            200,
            { "Content-Type": "application/json" },
            '{"columns":[\
                "fName",\
                "lName",\
                "Email",\
                "SSN",\
                "Phone",\
                "Address",\
                "City",\
                "State",\
                "ZIP",\
                "Preferred Contact Method",\
                "Highest Grade",\
                "Gender",\
                "Prior Service",\
                "Date Contacted",\
                "Notes"\
            ]}'
        );
    }
);

/* UPLOAD */
// post
// /api/upload
server.respondWith(
    "POST",
    /\/api\/validate/,
    function (xhr) {
        var args = arguments; server.check(args);
        xhr.respond(
            200,
            { "Content-Type": "application/json" },
            '{"duplicates":1,"invalid":2}'
        );
    }
);

/* PROCESS */
// post
// /api/process
server.respondWith(
    "POST",
    /\/api\/process/,
    function (xhr) {
        var args = arguments; server.check(args);
        xhr.respond(
            200,
            { "Content-Type": "application/json" },
            '{"total":333}'
        );
    }
);

/* GOALS */

// query
// list all favorited assets for this recruiter
// /api/recruiter/{recruiterID}/goal
server.respondWith(
    "GET",
    /\/api\/recruiter\/(\S+)\/goal/,
    function (xhr, rsid) {
        var args = arguments; server.check(args);
        xhr.respond(
            200,
            { "Content-Type": "application/json" },
            // [
                '{\
                    "monthly": {\
                        "contacts": 80,\
                        "scheduled": 30,\
                        "conducted": 25,\
                        "contracts": 15\
                    },\
                    "weekly": {\
                        "contacts": 30,\
                        "scheduled": 15,\
                        "conducted": 10,\
                        "contracts": 3\
                    }\
                }'
            // ]
        )
    }
);

// post
// /api/recruiter/{recruiterID}/goal
server.respondWith(
    "PUT",
    /\/api\/recruiter\/(\S+)\/goal/,
    function (xhr, rsid) {
        var args = arguments; server.check(args);
        xhr.respond(
            200,
            { "Content-Type": "application/json" },
            '{}'
        )
    }
);

/*
 * SESSION
 */

server.respondWith(
    "POST",
    /\/api\/session/,
    function (xhr) {
        // var args = arguments; server.check(args);
        // don't check for header

        // instead check for $_SERVER('CAC_CN')
        var request = $.parseJSON(xhr.requestBody) || {};
        if (
            // (!request.email && !request.password) ||
            (request.email === 'test@test.com' && request.password === 'test')
        ) {
            // console.log('responding 200!');
            xhr.respond(
                200,
                { "Content-Type": "application/json" },
                '{ "token" : "3847-BCDE-48CX-1808", "rsid" : "TNA" }'
            )
        } else {
            // console.log('responding 403!');
            xhr.respond(
                403,
                { "Content-Type": "application/json" },
                '{}'
            )
        }
        
    }
)

/*
 * TOOLBOX BOOKMARKS
 */


// query
// list all favorited assets for this recruiter
// /api/recruiter/{recruiterID}/asset-favorite
server.respondWith(
    "GET",
    /\/api\/recruiter\/(\S+)\/asset-favorite/,
    function (xhr, rsid) {
        var args = arguments; server.check(args);
        xhr.respond(
            200,
            { "Content-Type": "application/json" },
            '[\
                {\
                    "id": 200,\
                    "bookmark": true\
                },\
                {\
                    "id": 600,\
                    "bookmark": true\
                }\
            ]'
        )
    }
);

// create
// /api/recruiter/{recruiterID}/asset-favorite
server.respondWith(
    "POST",
    /\/api\/recruiter\/(\S+)\/asset-favorite/,
    function (xhr, id) {
        var args = arguments; server.check(args);
        xhr.respond(
            200,
            { "Content-Type": "application/json" },
            '{"id":'+400+'}'
        );
    }
);

// delete
// /api/recruiter/{recruiterID}/asset-favorite/{asset-id}
server.respondWith(
    "DELETE",
    /\/api\/recruiter\/(\S+)\/asset-favorite\/(\d+)/,
    function (xhr, rsid, id) {
        var args = arguments; server.check(args);
        xhr.respond(
            200,
            { "Content-Type": "application/json" },
            '{"id":'+id+'}'
        );
    }
);


/*
 * SAVED SEARCHES
 */


// query
// list all saved searches for this recruiter
// /api/recruiter/{recruiterID}/saved-search
server.respondWith(
    "GET",
    /\/api\/recruiter\/(\S+)\/saved-search/,
    function (xhr, uid) {
        var allSavedSearches = [
            {
                id: 1,
                name: 'test1',
                vilpacStatus: 'lead',
                query: 'Redd',
                orderProp: ['-age','','']
            },
            {
                id: 2,
                name: 'test2',
                vilpacStatus: 'processing',
                query: 'Westbrook',
                orderProp: ['','','']
            }
        ];
        var params = URI(xhr.url).search(true);

        var query = angular.copy(params.query);

        delete params.sinon;
        var savedSearches = params ?
            _.where(allSavedSearches, params) :
            allSavedSearches;

        xhr.respond(
            200,
            { "Content-Type": "application/json" },
            JSON.stringify(savedSearches)
        );
    }
);

//
// update/rename
// /api/recruiter/{recruiterID}/saved-search/{searchID}
server.respondWith(
    "PUT",
    /\/api\/recruiter\/(\S+)\/saved-search\/(\S+)/,
    function (xhr, uid) {
        xhr.respond(
            200,
            { "Content-Type": "application/json" },
            ''
        );
    }
);

//
// delete
// /api/recruiter/{recruiterID}/saved-search/{searchID}
server.respondWith(
    "DELETE",
    /\/api\/recruiter\/(\S+)\/saved-search\/(\S+)/,
    function (xhr, uid) {
        xhr.respond(
            200,
            { "Content-Type": "application/json" },
            ''
        );
    }
);

//
// save
// /api/recruiter/{recruiterID}/saved-search/{searchID}
server.respondWith(
    "POST",
    /\/api\/recruiter\/(\S+)\/saved-search/,
    function (xhr, uid) {
        var args = arguments; server.check(args);
        xhr.respond(
            200,
            { "Content-Type": "application/json" },
            '{"uid":300}'
        );
    }
);


/*
 * RECRUITERS
 */


// get
// /api/recruiter/{recruiterID}
// get a recruiter by ID
server.respondWith(
    "GET",
    /\/api\/recruiter\/(\S+)/,
    function (xhr, rsid) {
        var args = arguments; server.check(args);
        xhr.respond(
            200,
            { "Content-Type": "application/json" },
            '{"uid":'+ 500 + ',\
            "rsid": "' + rsid + '",\
            "title": "SGT",\
            "firstName": "Michael",\
            "lastName": "Puckett",\
            "userRole": "admin",\
            "office": 6152091380,\
            "cell": 5025008140,\
            "email": "michael.c.puckett@us.army.mil",\
            "city": "Nashville",\
            "state": "tn"}'
        );
    }
);

// query
// /api/recruiter
// get list of recruiter
server.respondWith(
    "GET",
    /\/api\/recruiter/,
    function (xhr, rsid) {
        var args = arguments; server.check(args);
        xhr.respond(
            200,
            { "Content-Type": "application/json" },
            '[\
                {\
                    "uid":'+ 500 + ',\
                    "rsid": "TNA",\
                    "title": "SGT",\
                    "firstName": "Michael",\
                    "lastName": "Puckett",\
                    "userRole": "admin",\
                    "office": 6152091380,\
                    "cell": 5025008140,\
                    "email": "michael.c.puckett@us.army.mil",\
                    "city": "Nashville",\
                    "state": "tn"\
                },{\
                    "uid":'+ 400 + ',\
                    "rsid": "DCB",\
                    "title": "SGT",\
                    "firstName": "Kelly",\
                    "lastName": "Redd",\
                    "userRole": "admin",\
                    "office": 6152091380,\
                    "cell": 5025008140,\
                    "email": "michael.c.puckett@us.army.mil",\
                    "city": "Nashville",\
                    "state": "tn"\
                }\
            ]'
        );
    }
);

//            "bio": "Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit. Etiam porta sem malesuada magna mollis ultricies vehi  euismod. Curabitur blandit tempus porttitor.\
                // Curabitur blandit tempus porttitor. Fusce dapibus, tellus ac cursus commodo, tortor mauris et uhelc ondimentum nibh, fermentum massa justo sit amet risus. Aenean lacinia bibendum nulla sed consectetur. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Nullam quis risus eget urna mollis ornare vel eu leo.\
                // Nulla vitae elit libero, a pharetra augue. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper."}'

/*
 * CONTACTS
 */

// recruiter contact history
server.respondWith(
    "GET",
    /\/api\/recruiter\/(\S+)\/contact-history/,
    function (xhr, uid) {
        var args = arguments; server.check(args);
        xhr.respond(
            200,
            { "Content-Type": "application/json" },
            '['+
                '{\
                    "id":100,\
                    "type": "appointment",\
                    "notes": "1For home visit",\
                    "time": "' + moment().subtract('days', 1).utc().format('YYYY-MM-DDTHH:mm:ssZZ') + '"\
                },\
                {\
                    "id": 200,\
                    "type": "message",\
                    "notes": "7Asked for visit",\
                    "time": "' + moment().subtract('days', 7).utc().format('YYYY-MM-DDTHH:mm:ssZZ') + '"\
                },\
                {\
                    "id": 300,\
                    "type": "spoke",\
                    "notes": "6Met at high school assembly",\
                    "time": "' + moment().subtract('days', 6).utc().format('YYYY-MM-DDTHH:mm:ssZZ') + '"\
                },\
                {\
                    "id": 400,\
                    "type": "message",\
                    "notes": "2hrs - Asked for visit",\
                    "time": "' + moment().subtract('hours', 2).utc().format('YYYY-MM-DDTHH:mm:ssZZ') + '"\
                },\
                {\
                    "id": 500,\
                    "type": "message",\
                    "notes": "7Asked for visit",\
                    "time": "' + moment().subtract('days', 7).utc().format('YYYY-MM-DDTHH:mm:ssZZ') + '"\
                },\
                {\
                    "id": 600,\
                    "type": "message",\
                    "notes": "3Asked for visit",\
                    "time": "' + moment().subtract('days', 3).utc().format('YYYY-MM-DDTHH:mm:ssZZ') + '"\
                },\
                {\
                    "id": 700,\
                    "type": "message",\
                    "notes": "4Asked for visit",\
                    "time": "' + moment().subtract('days', 4).utc().format('YYYY-MM-DDTHH:mm:ssZZ') + '"\
                }\
            ]'
        );
    }
);



// query
server.respondWith(
    "GET",
    /\/api\/candidate\/(\S+)\/contact-history/,
    function (xhr, personID) {
        var args = arguments; server.check(args);
        xhr.respond(
            200,
            { "Content-Type": "application/json" },
            '[\
                {\
                    "id":100,\
                    "personID": "' + personID + '",\
                    "rsid": "TN",\
                    "type": "appointment",\
                    "notes": "For home visit",\
                    "time": "' + moment().subtract('days', 4).utc().format('YYYY-MM-DDTHH:mm:ssZZ') + '"\
                },\
                {\
                    "id": 200,\
                    "personID": "' + personID + '",\
                    "rsid": "TN",\
                    "type": "message",\
                    "notes": "Asked for visit",\
                    "time": "' + moment().subtract('days', 7).utc().format('YYYY-MM-DDTHH:mm:ssZZ') + '"\
                },\
                {\
                    "id": 300,\
                    "personID": "' + personID + '",\
                    "rsid": "TN",\
                    "type": "spoke",\
                    "notes": "Met at high school assembly",\
                    "time": "' + moment().subtract('days', 14).utc().format('YYYY-MM-DDTHH:mm:ssZZ') + '"\
                }\
            ]'
        );
    }
);

// add
server.respondWith(
    "POST",
    /\/api\/contact/,
    function (xhr, uid) {
        var args = arguments; server.check(args);
        xhr.respond(
            200,
            { "Content-Type": "application/json" },
            '{"uid":300}'
        );
    }
);

/*
 * FILES
 */

 // query
 server.respondWith(
    "GET",
    /\/api\/asset/,
    function (xhr, uid) {
        var args = arguments; server.check(args);
        xhr.respond(
            200,
            { "Content-Type": "application/json" },
            '[\
                {\
                    "id":100,\
                    "title": "Regulatory Documents",\
                    "type": ".pdf",\
                    "tags": [\
                        "admin"\
                    ]\
                },\
                {\
                    "id":200,\
                    "title": "USPS",\
                    "type": ".xls",\
                    "tags": [\
                        "billing",\
                        "admin",\
                        "accounting"\
                    ]\
                },\
                {\
                    "id":300,\
                    "title": "RRNCO Apple-DMD",\
                    "type": ".xls",\
                    "tags": [\
                        "info",\
                        "data"\
                    ]\
                },\
                {\
                    "id":400,\
                    "title": "RRNCO Apple-DMD",\
                    "type": ".xls",\
                    "tags": [\
                        "info",\
                        "data"\
                    ]\
                },\
                {\
                    "id":500,\
                    "title": "RRNCO Apple-DMD",\
                    "type": ".xls",\
                    "tags": [\
                        "info",\
                        "data"\
                    ]\
                },\
                {\
                    "id":600,\
                    "title": "RRNCO Apple-DMD",\
                    "type": ".xls",\
                    "tags": [\
                        "info",\
                        "data"\
                    ]\
                },\
                {\
                    "id":700,\
                    "title": "RRNCO Apple-DMD",\
                    "type": ".xls",\
                    "tags": [\
                        "info",\
                        "data"\
                    ]\
                },\
                {\
                    "id":800,\
                    "title": "RRNCO Apple-DMD",\
                    "type": ".xls",\
                    "tags": [\
                        "info",\
                        "data"\
                    ]\
                },\
                {\
                    "id":900,\
                    "title": "RRNCO Apple-DMD",\
                    "type": ".xls",\
                    "tags": [\
                        "info",\
                        "data"\
                    ]\
                },\
                {\
                    "id":1000,\
                    "title": "RRNCO Apple-DMD",\
                    "type": ".xls",\
                    "tags": [\
                        "info",\
                        "data"\
                    ]\
                },\
                {\
                    "id":1100,\
                    "title": "RRNCO Apple-DMD",\
                    "type": ".xls",\
                    "tags": [\
                        "info",\
                        "data"\
                    ]\
                },\
                {\
                    "id":1200,\
                    "title": "RRNCO Apple-DMD",\
                    "type": ".xls",\
                    "tags": [\
                        "info",\
                        "data"\
                    ]\
                },\
                {\
                    "id":1300,\
                    "title": "RRNCO Apple-DMD",\
                    "type": ".xls",\
                    "tags": [\
                        "info",\
                        "data"\
                    ]\
                },\
                {\
                    "id":1400,\
                    "title": "RRNCO Apple-DMD",\
                    "type": ".xls",\
                    "tags": [\
                        "info",\
                        "data"\
                    ]\
                },\
                {\
                    "id":1500,\
                    "title": "RRNCO Apple-DMD",\
                    "type": ".xls",\
                    "tags": [\
                        "info",\
                        "data"\
                    ]\
                }\
            ]'
        );
    }
);

// create
server.respondWith(
    "POST",
    /\/api\/asset/,
    function (xhr) {
        var args = arguments; server.check(args);
        xhr.respond(
            200,
            { "Content-Type": "application/json" },
            '{"id":205}'
        );
    }
);

// save
server.respondWith(
    "PUT",
    /\/api\/asset\/(\d+)/,
    function (xhr, id) {
        var args = arguments; server.check(args);
        xhr.respond(
            200,
            { "Content-Type": "application/json" },
            '{"id":'+ id + '}'
        );
    }
);

// delete
server.respondWith(
    "DELETE",
    /\/api\/asset\/(\d+)/,
    function (xhr, id) {
        var args = arguments; server.check(args);
        xhr.respond(
            200,
            { "Content-Type": "application/json" },
            '{"uid":'+ id + '}'
        );
    }
);


/*
 * APPOINTMENTS
 */

 // query
 server.respondWith(
    "GET",
    /\/api\/appointment/,
    function (xhr) {
        var args = arguments; server.check(args);
        xhr.respond(
            200,
            { "Content-Type": "application/json" },
            '[\
                {\
                    "id": 100,\
                    "title":"Today Appointment",\
                    "time": "' + moment().utc().format('YYYY-MM-DDTHH:mm:ssZZ') + '",\
                    "type": "Face to Face",\
                    "personID": 200\
                },\
                {\
                    "id": 200,\
                    "title":"Appointment #2",\
                    "time": "' + moment().add('days', 1).utc().format('YYYY-MM-DDTHH:mm:ssZZ') + '",\
                    "type": "MEMPS",\
                    "personID": 200\
                },\
                {\
                    "id": 300,\
                    "title":"Appointment #3",\
                    "time": "' + moment().add('days', 4).utc().format('YYYY-MM-DDTHH:mm:ssZZ') + '",\
                    "type": "ASVAB",\
                    "personID": 200\
                }\
            ]'
        );
    }
);

// create
server.respondWith(
    "POST",
    /\/api\/appointment/,
    function (xhr) {
        var args = arguments; server.check(args);
        xhr.respond(
            200,
            { "Content-Type": "application/json" },
            '{"id":205}'
        );
    }
);

 /*
 * EVENTS
 */

 // query
 server.respondWith(
    "GET",
    /\/api\/event/,
    function (xhr) {
        var args = arguments; server.check(args);
        xhr.respond(
            200,
            { "Content-Type": "application/json" },
            '[\
                {\
                    "id": 100,\
                    "title":"Today Event",\
                    "time": "' + moment().utc().format('YYYY-MM-DDTHH:mm:ssZZ') + '"\
                },\
                {\
                    "id": 200,\
                    "title":"Event #2",\
                    "time": "' + moment().add('days', 1).utc().format('YYYY-MM-DDTHH:mm:ssZZ') + '"\
                },\
                {\
                    "id": 300,\
                    "title":"Event #3",\
                    "time": "' + moment().add('days', 4).utc().format('YYYY-MM-DDTHH:mm:ssZZ') + '"\
                }\
            ]'
        );
    }
);

 // create
server.respondWith(
    "POST",
    /\/api\/event/,
    function (xhr) {
        var args = arguments; server.check(args);
        xhr.respond(
            200,
            { "Content-Type": "application/json" },
            '{"id":205}'
        );
    }
);

/*
 * CANDIDATES
 */

// get
server.respondWith(
    "GET",
    /\/api\/candidate\/(\S+)/,
    function (xhr, personID) {
        var args = arguments; server.check(args);
        xhr.respond(
            200,
            { "Content-Type": "application/json" },
            '{"personID":'+ personID + ',\
            "firstName": "Michael",\
            "lastName": "Puckett",\
            "address1": "1712 Villa Place",\
            "city": "Nashville",\
            "state": "TN",\
            "zip": "37212",\
            "email": "michaelcpuckett@gmail.com",\
            "age": "25",\
            "phone": "6152091380",\
            "contacted": true,\
            "registered": "2012-06-14",\
            "lastAction": "2012-07-20",\
            "prior": false,\
            "packet": "95%",\
            "appointment": "2012-08-01"}'
        );
    }
);

// query
server.respondWith(
    "GET",
    /\/api\/candidate/,
    function (xhr, uid) {
        var allCandidates = [
            {
                "personID": "bd534077-7357-4ebf-bc5a-247fae95e4c4",
                "firstName": "Michael",
                "lastName": "Puckett",
                "address1": "1712 Villa Place",
                "city": "Nashville",
                "state": "TN",
                "zip": "37212",
                "email": "michaelcpuckett@gmail.com",
                "age": "25",
                "phone": "6152091380",
                "contacted": true,
                "registered": "2012-06-14",
                "lastAction": moment().subtract('days', 2).utc().format('YYYY-MM-DDTHH:mm:ssZZ'),
                "prior": false,
                "packet": "95%",
                "appointment": "2012-08-01",
                "status": "goGuard",
                "vilpacStatus": "lead"
            },
            {
                "personID": "bd534077-7357-4ebf-bc5a-247fae95e4c4",
                "firstName": "Michael",
                "lastName": "Puckett",
                "address1": "1712 Villa Place",
                "city": "Abcdefg",
                "state": "TN",
                "zip": "37212",
                "email": "michaelcpuckett@gmail.com",
                "age": "25",
                "phone": "6152091380",
                "contacted": true,
                "registered": "2012-06-14",
                "lastAction": moment().subtract('days', 14).utc().format('YYYY-MM-DDTHH:mm:ssZZ'),
                "prior": false,
                "packet": "95%",
                "appointment": "2012-08-01",
                "status": "qualified",
                "vilpacStatus": "lead"
            },
            {
                "personID": "bd534077-7357-4ebf-bc5a-247fae95e4c4",
                "firstName": "Hollye",
                "lastName": "Westbrook",
                "address1": "624 Cherry Alley",
                "address2": "Apt A",
                "city": "Bowling Green",
                "state": "KY",
                "zip": "42101",
                "email": "hollyewestbrook@gmail.com",
                "age": "25",
                "phone": "2702091380",
                "contacted": false,
                "registered": "2011-06-14",
                "lastAction": moment().subtract('days', 24).utc().format('YYYY-MM-DDTHH:mm:ssZZ'),
                "prior": false,
                "packet": "35%",
                "appointment": "2011-08-01",
                "status": "generated",
                "vilpacStatus": "lead"
            },
            {
                "personID": "bd534077-7357-4ebf-bc5a-247fae95e4c4",
                "firstName": "Chester",
                "lastName": "Test",
                "address1": "624 Cherry Alley",
                "address2": "Apt A",
                "city": "Bowling Green",
                "state": "KY",
                "zip": "42101",
                "email": "hollyewestbrook@gmail.com",
                "age": "25",
                "phone": "2702091380",
                "contacted": false,
                "registered": "2011-06-14",
                "lastAction": moment().subtract('days', 2).utc().format('YYYY-MM-DDTHH:mm:ssZZ'),
                "prior": false,
                "packet": "35%",
                "appointment": "2011-08-01",
                "status": "goGuard",
                "vilpacStatus": "lead"
            },
            {
                "personID": "bd534077-7357-4ebf-bc5a-247fae95e4c4",
                "firstName": "Hollye",
                "lastName": "Westbrook",
                "address1": "624 Cherry Alley",
                "address2": "Apt A",
                "city": "Bowling Green",
                "state": "KY",
                "zip": "42101",
                "email": "hollyewestbrook@gmail.com",
                "age": "25",
                "phone": "2702091380",
                "contacted": false,
                "registered": "2011-06-14",
                "lastAction": moment().subtract('days', 8).utc().format('YYYY-MM-DDTHH:mm:ssZZ'),
                "prior": false,
                "packet": "35%",
                "appointment": "2011-08-01",
                "status": "unscreened",
                "vilpacStatus": "lead"
            },
            {
                "personID": "bd534077-7357-4ebf-bc5a-247fae95e4c4",
                "firstName": "Kelly",
                "lastName": "Redd",
                "address1": "624 Cherry Alley",
                "address2": "Apt A",
                "city": "Bowling Green",
                "state": "KY",
                "zip": "42101",
                "email": "hollyewestbrook@gmail.com",
                "age": "25",
                "phone": "2702091380",
                "contacted": false,
                "registered": "2011-06-14",
                "lastAction": moment().subtract('days', 7).utc().format('YYYY-MM-DDTHH:mm:ssZZ'),
                "prior": false,
                "packet": "99%",
                "appointment": "2011-08-01",
                "status": "generated",
                "vilpacStatus": "lead"
            },
            {
                "personID": "bd534077-7357-4ebf-bc5a-247fae95e4c4",
                "firstName": "Kelly",
                "lastName": "Redd",
                "address1": "624 Cherry Alley",
                "address2": "Apt A",
                "city": "Bowling Green",
                "state": "KY",
                "zip": "42101",
                "email": "hollyewestbrook@gmail.com",
                "age": "25",
                "phone": "2702091380",
                "contacted": false,
                "registered": "2011-06-14",
                "lastAction": moment().subtract('days', 6).utc().format('YYYY-MM-DDTHH:mm:ssZZ'),
                "prior": false,
                "packet": "99%",
                "appointment": "2011-08-01",
                "status": "generated",
                "vilpacStatus": "contract"
            },
            {
                "personID": "bd534077-7357-4ebf-bc5a-247fae95e4c4",
                "firstName": "Chester",
                "lastName": "Test",
                "address1": "624 Cherry Alley",
                "address2": "Apt A",
                "city": "Bowling Green",
                "state": "KY",
                "zip": "42101",
                "email": "hollyewestbrook@gmail.com",
                "age": "25",
                "phone": "2702091380",
                "contacted": false,
                "registered": "2011-06-14",
                "lastAction": moment().subtract('days', 5).utc().format('YYYY-MM-DDTHH:mm:ssZZ'),
                "prior": false,
                "packet": "35%",
                "appointment": "2011-08-01",
                "status": "goGuard",
                "vilpacStatus": "processing"
            },
            {
                "personID": "bd534077-7357-4ebf-bc5a-247fae95e4c4",
                "firstName": "Hollye",
                "lastName": "Westbrook",
                "address1": "624 Cherry Alley",
                "address2": "Apt A",
                "city": "Bowling Green",
                "state": "KY",
                "zip": "42101",
                "email": "hollyewestbrook@gmail.com",
                "age": "25",
                "phone": "2702091380",
                "contacted": false,
                "registered": "2011-06-14",
                "lastAction": moment().subtract('days', 12).utc().format('YYYY-MM-DDTHH:mm:ssZZ'),
                "prior": false,
                "packet": "35%",
                "appointment": "2011-08-01",
                "status": "unscreened",
                "vilpacStatus": "processing"
            },
            {
                "personID": "bd534077-7357-4ebf-bc5a-247fae95e4c4",
                "firstName": "Kelly",
                "lastName": "Redd",
                "address1": "624 Cherry Alley",
                "address2": "Apt A",
                "city": "Bowling Green",
                "state": "KY",
                "zip": "42101",
                "email": "hollyewestbrook@gmail.com",
                "age": "25",
                "phone": "2702091380",
                "contacted": false,
                "registered": "2011-06-14",
                "lastAction": moment().subtract('days', 11).utc().format('YYYY-MM-DDTHH:mm:ssZZ'),
                "prior": false,
                "packet": "99%",
                "appointment": "2011-08-01",
                "status": "generated",
                "vilpacStatus": "contract"
            },
            {
                "personID": "bd534077-7357-4ebf-bc5a-247fae95e4c4",
                "firstName": "Kelly",
                "lastName": "Redd",
                "address1": "624 Cherry Alley",
                "address2": "Apt A",
                "city": "Bowling Green",
                "state": "KY",
                "zip": "42101",
                "email": "hollyewestbrook@gmail.com",
                "age": "25",
                "phone": "2702091380",
                "contacted": false,
                "registered": "2011-06-14",
                "lastAction": moment().subtract('days', 2).utc().format('YYYY-MM-DDTHH:mm:ssZZ'),
                "prior": false,
                "packet": "99%",
                "appointment": "2011-08-01",
                "status": "generated",
                "vilpacStatus": "contract"
            },
            {
                "personID": "bd534077-7357-4ebf-bc5a-247fae95e4c4",
                "firstName": "Chester",
                "lastName": "Test",
                "address1": "624 Cherry Alley",
                "address2": "Apt A",
                "city": "Bowling Green",
                "state": "KY",
                "zip": "42101",
                "email": "hollyewestbrook@gmail.com",
                "age": "25",
                "phone": "2702091380",
                "contacted": false,
                "registered": "2011-06-14",
                "lastAction": moment().subtract('days', 17).utc().format('YYYY-MM-DDTHH:mm:ssZZ'),
                "prior": false,
                "packet": "35%",
                "appointment": "2011-08-01",
                "status": "goGuard",
                "vilpacStatus": "processing"
            },
            {
                "personID": "bd534077-7357-4ebf-bc5a-247fae95e4c4",
                "firstName": "Hollye",
                "lastName": "Westbrook",
                "address1": "624 Cherry Alley",
                "address2": "Apt A",
                "city": "Bowling Green",
                "state": "KY",
                "zip": "42101",
                "email": "hollyewestbrook@gmail.com",
                "age": "25",
                "phone": "2702091380",
                "contacted": false,
                "registered": "2011-06-14",
                "lastAction": moment().subtract('days', 3).utc().format('YYYY-MM-DDTHH:mm:ssZZ'),
                "prior": false,
                "packet": "35%",
                "appointment": "2011-08-01",
                "status": "unscreened",
                "vilpacStatus": "processing"
            },
            {
                "personID": "bd534077-7357-4ebf-bc5a-247fae95e4c4",
                "firstName": "Kelly",
                "lastName": "Redd",
                "address1": "624 Cherry Alley",
                "address2": "Apt A",
                "city": "Bowling Green",
                "state": "KY",
                "zip": "42101",
                "email": "hollyewestbrook@gmail.com",
                "age": "25",
                "phone": "2702091380",
                "contacted": false,
                "registered": "2011-06-14",
                "lastAction": moment().subtract('days', 14).utc().format('YYYY-MM-DDTHH:mm:ssZZ'),
                "prior": false,
                "packet": "99%",
                "appointment": "2011-08-01",
                "status": "generated",
                "vilpacStatus": "contract"
            },
            {
                "personID": "bd534077-7357-4ebf-bc5a-247fae95e4c4",
                "firstName": "Kelly",
                "lastName": "Redd",
                "address1": "624 Cherry Alley",
                "address2": "Apt A",
                "city": "Bowling Green",
                "state": "KY",
                "zip": "42101",
                "email": "hollyewestbrook@gmail.com",
                "age": "25",
                "phone": "2702091380",
                "contacted": false,
                "registered": "2011-06-14",
                "lastAction": moment().subtract('days', 14).utc().format('YYYY-MM-DDTHH:mm:ssZZ'),
                "prior": false,
                "packet": "99%",
                "appointment": "2011-08-01",
                "status": "generated",
                "vilpacStatus": "contract"
            },
            {
                "personID": "bd534077-7357-4ebf-bc5a-247fae95e4c4",
                "firstName": "Chester",
                "lastName": "Test",
                "address1": "624 Cherry Alley",
                "address2": "Apt A",
                "city": "Bowling Green",
                "state": "KY",
                "zip": "42101",
                "email": "hollyewestbrook@gmail.com",
                "age": "25",
                "phone": "2702091380",
                "contacted": false,
                "registered": "2011-06-14",
                "lastAction": moment().subtract('days', 2).utc().format('YYYY-MM-DDTHH:mm:ssZZ'),
                "prior": false,
                "packet": "35%",
                "appointment": "2011-08-01",
                "status": "goGuard",
                "vilpacStatus": "processing"
            },
            {
                "personID": "bd534077-7357-4ebf-bc5a-247fae95e4c4",
                "firstName": "Hollye",
                "lastName": "Westbrook",
                "address1": "624 Cherry Alley",
                "address2": "Apt A",
                "city": "Bowling Green",
                "state": "KY",
                "zip": "42101",
                "email": "hollyewestbrook@gmail.com",
                "age": "25",
                "phone": "2702091380",
                "contacted": false,
                "registered": "2011-06-14",
                "lastAction": moment().subtract('days', 1).utc().format('YYYY-MM-DDTHH:mm:ssZZ'),
                "prior": false,
                "packet": "35%",
                "appointment": "2011-08-01",
                "status": "unscreened",
                "vilpacStatus": "lead"
            },
            {
                "personID": "bd534077-7357-4ebf-bc5a-247fae95e4c4",
                "firstName": "Kelly",
                "lastName": "Redd",
                "address1": "624 Cherry Alley",
                "address2": "Apt A",
                "city": "Bowling Green",
                "state": "KY",
                "zip": "42101",
                "email": "hollyewestbrook@gmail.com",
                "age": "25",
                "phone": "2702091380",
                "contacted": false,
                "registered": "2011-06-14",
                "lastAction": moment().subtract('days', 7).utc().format('YYYY-MM-DDTHH:mm:ssZZ'),
                "prior": false,
                "packet": "99%",
                "appointment": "2011-08-01",
                "status": "goGuard",
                "vilpacStatus": "contract"
            },
            {
                "personID": "bd534077-7357-4ebf-bc5a-247fae95e4c4",
                "firstName": "Kelly",
                "lastName": "Redd",
                "address1": "624 Cherry Alley",
                "address2": "Apt A",
                "city": "Bowling Green",
                "state": "KY",
                "zip": "42101",
                "email": "hollyewestbrook@gmail.com",
                "age": "25",
                "phone": "2702091380",
                "contacted": false,
                "registered": "2011-06-14",
                "lastAction": moment().subtract('days', 5).utc().format('YYYY-MM-DDTHH:mm:ssZZ'),
                "prior": false,
                "packet": "99%",
                "appointment": "2011-08-01",
                "status": "generated",
                "vilpacStatus": "contract"
            },
            {
                "personID": "bd534077-7357-4ebf-bc5a-247fae95e4c4",
                "firstName": "Chester",
                "lastName": "Test",
                "address1": "624 Cherry Alley",
                "address2": "Apt A",
                "city": "Bowling Green",
                "state": "KY",
                "zip": "42101",
                "email": "hollyewestbrook@gmail.com",
                "age": "25",
                "phone": "2702091380",
                "contacted": false,
                "registered": "2011-06-14",
                "lastAction": moment().subtract('days', 7).utc().format('YYYY-MM-DDTHH:mm:ssZZ'),
                "prior": false,
                "packet": "35%",
                "appointment": "2011-08-01",
                "status": "goGuard",
                "vilpacStatus": "processing"
            },
            {
                "personID": "bd534077-7357-4ebf-bc5a-247fae95e4c4",
                "firstName": "Hollye",
                "lastName": "Westbrook",
                "address1": "624 Cherry Alley",
                "address2": "Apt A",
                "city": "Bowling Green",
                "state": "KY",
                "zip": "42101",
                "email": "hollyewestbrook@gmail.com",
                "age": "25",
                "phone": "2702091380",
                "contacted": false,
                "registered": "2011-06-14",
                "lastAction": moment().subtract('days', 11).utc().format('YYYY-MM-DDTHH:mm:ssZZ'),
                "prior": false,
                "packet": "35%",
                "appointment": "2011-08-01",
                "status": "generated",
                "vilpacStatus": "processing"
            },
            {
                "personID": "bd534077-7357-4ebf-bc5a-247fae95e4c4",
                "firstName": "Kelly",
                "lastName": "Redd",
                "address1": "624 Cherry Alley",
                "address2": "Apt A",
                "city": "Bowling Green",
                "state": "KY",
                "zip": "42101",
                "email": "hollyewestbrook@gmail.com",
                "age": "25",
                "phone": "2702091380",
                "contacted": false,
                "registered": "2011-06-14",
                "lastAction": moment().subtract('days', 18).utc().format('YYYY-MM-DDTHH:mm:ssZZ'),
                "prior": false,
                "packet": "99%",
                "appointment": "2011-08-01",
                "status": "generated",
                "vilpacStatus": "contract"
            },
            {
                "personID": "bd534077-7357-4ebf-bc5a-247fae95e4c4",
                "firstName": "Kelly",
                "lastName": "Redd",
                "address1": "624 Cherry Alley",
                "address2": "Apt A",
                "city": "Bowling Green",
                "state": "KY",
                "zip": "42101",
                "email": "hollyewestbrook@gmail.com",
                "age": "25",
                "phone": "2702091380",
                "contacted": false,
                "registered": "2011-06-14",
                "lastAction": moment().subtract('days', 4).utc().format('YYYY-MM-DDTHH:mm:ssZZ'),
                "prior": false,
                "packet": "99%",
                "appointment": "2011-08-01",
                "status": "generated",
                "vilpacStatus": "contract"
            },
            {
                "personID": "bd534077-7357-4ebf-bc5a-247fae95e4c4",
                "firstName": "Chester",
                "lastName": "Test",
                "address1": "624 Cherry Alley",
                "address2": "Apt A",
                "city": "Bowling Green",
                "state": "KY",
                "zip": "42101",
                "email": "hollyewestbrook@gmail.com",
                "age": "25",
                "phone": "2702091380",
                "contacted": false,
                "registered": "2011-06-14",
                "lastAction": moment().subtract('days', 1).utc().format('YYYY-MM-DDTHH:mm:ssZZ'),
                "prior": false,
                "packet": "35%",
                "appointment": "2011-08-01",
                "status": "goGuard",
                "vilpacStatus": "lead"
            },
            {
                "personID": "bd534077-7357-4ebf-bc5a-247fae95e4c4",
                "firstName": "Hollye",
                "lastName": "Westbrook",
                "address1": "624 Cherry Alley",
                "address2": "Apt A",
                "city": "Bowling Green",
                "state": "KY",
                "zip": "42101",
                "email": "hollyewestbrook@gmail.com",
                "age": "25",
                "phone": "2702091380",
                "contacted": false,
                "registered": "2011-06-14",
                "lastAction": moment().subtract('days', 9).utc().format('YYYY-MM-DDTHH:mm:ssZZ'),
                "prior": false,
                "packet": "35%",
                "appointment": "2011-08-01",
                "status": "qualified",
                "vilpacStatus": "processing"
            },
            {
                "personID": "bd534077-7357-4ebf-bc5a-247fae95e4c4",
                "firstName": "Kelly",
                "lastName": "Redd",
                "address1": "624 Cherry Alley",
                "address2": "Apt A",
                "city": "Bowling Green",
                "state": "KY",
                "zip": "42101",
                "email": "hollyewestbrook@gmail.com",
                "age": "25",
                "phone": "2702091380",
                "contacted": false,
                "registered": "2011-06-14",
                "lastAction": moment().subtract('days', 8).utc().format('YYYY-MM-DDTHH:mm:ssZZ'),
                "prior": false,
                "packet": "99%",
                "appointment": "2011-08-01",
                "status": "generated",
                "vilpacStatus": "lead"
            },
            {
                "personID": "bd534077-7357-4ebf-bc5a-247fae95e4c4",
                "firstName": "Kelly",
                "lastName": "Redd",
                "address1": "624 Cherry Alley",
                "address2": "Apt A",
                "city": "Bowling Green",
                "state": "KY",
                "zip": "42101",
                "email": "hollyewestbrook@gmail.com",
                "age": "25",
                "phone": "2702091380",
                "contacted": false,
                "registered": "2011-06-14",
                "lastAction": moment().subtract('days', 7).utc().format('YYYY-MM-DDTHH:mm:ssZZ'),
                "prior": false,
                "packet": "99%",
                "appointment": "2011-08-01",
                "status": "qualified",
                "vilpacStatus": "lead"
            }
        ];

        var params = URI(xhr.url).search(true);

        var query = angular.copy(params.query);

        delete params.sinon;
        delete params.limit;
        delete params.page;
        delete params.orderProp;
        delete params.query;
        delete params.name;
        delete params.id;

        var candidates = params ?
            _.where(allCandidates, params) :
            allCandidates;

        if (query) {
            candidates = _.filter(candidates, function (candidate) {
                var values = _.values(candidate);
                return _.contains(values, query);// === key.toString().toLowerCase();
            })
        }

        var range = xhr.requestHeaders['Range'] || '0-25';
        var max = range.split('-')[1];

        _.first(candidates, max);

        var args = arguments; server.check(args);
        xhr.respond(
            200,
            {
                'Content-Type': 'application/json',
                'Content-Range': range + '/100'
            },
            JSON.stringify(candidates)
        );
    }
);


// add
server.respondWith(
    "POST",
    /\/api\/candidate/,
    function (xhr, uid) {
        var args = arguments; server.check(args);
        xhr.respond(
            200,
            { "Content-Type": "application/json" },
            '{"uid":300}'
        );
    }
);