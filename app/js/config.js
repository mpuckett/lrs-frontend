/* Configure Providers */
lrs.config(function($routeProvider, $locationProvider, $httpProvider, API) {

    // give API direct access to manipulate headers
    API.INIT($httpProvider.defaults.headers.common, 'Auth-Token');
    $httpProvider.defaults.headers.common['Access-Control-Allow-Origin'] = '*';

    $httpProvider.responseInterceptors.push(function($q, NotificationService, $location) {
        return function(promise) {
            return promise.then(function(response) {
                return response;
            }, function (response) {
                if (response.status === 403) {
                     response.data = 'You need to register your CAC when logging in with your password before you can login with your CAC.';
                }
                if ($location.path() !== '/login' && !response.config.preventDefaultError) {
                    // global notifier
                    // will redirect to login page from SessionService listener
                    switch (response.status) {
                        case 403:
                        NotificationService.reportUnauthorized(response);
                        break;
                        default:
                        NotificationService.reportFailure(response);
                    }
                }
                return $q.reject(response);
            });
        }
});

  // $httpProvider.defaults.transformRequest = function(data){
  //   console.log(data, arguments);
  //       if( data ){
  //           console.log('data', data);
  //           return $.param(data);
  //       }
  //   };

  // console.log($httpProvider);

  // var current = angular.copy($httpProvider.defaults.transformResponse[0]);

  // $httpProvider.defaults.transformResponse = [function (data) {
  //   console.log(data);
  //   console.log(current(data));
  // }];

  // $locationProvider.html5Mode(true);

});