function ModalCtrl($scope, $rootScope, SessionService) {
	$scope.shown = false;

	$rootScope.$on('alert', function (e, response) {
		$scope.data = response.data;
		$scope.partial = response.partial;
		$scope.deferred = response.deferred;
		$scope.shown = true;
	});

	$rootScope.$on('success', function (e, response) {
		$scope.data = response;
		$scope.partial = '/partials/modal/success.html';
		$scope.shown = true;
	});

	$rootScope.$on('failure', function (e, response) {
		$scope.data = response;
		$scope.partial = '/partials/modal/failure.html';
		$scope.shown = true;
	});

	$rootScope.$on('unauthorized', function (e,response) {
		$scope.data = response;
		$scope.partial = '/partials/modal/unauthorized.html';
		$scope.shown = true;
	});

	// clear modal on close
	$scope.$watch('shown', function () {
		// TODO: Is this really clearing?
		if (!$scope.shown) {
			$scope.data = {};
			$scope.partial = '';
			$scope.deferred = null;
		}
	});

	$scope.close = function () {
		$scope.shown = false;
	}
}