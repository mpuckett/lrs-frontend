function CandidateContactDetailsCtrl($scope, ContactService, CandidateService) {
	$scope.candidate = CandidateService.get({ personID: $scope.data.personID })
	.then(function(response) {
		return response.data;
	});
	$scope.contacts = ContactService.getCandidateContactHistory({ personID: $scope.data.personID })
	.then(function (response) {
		return response.data;
	});
}