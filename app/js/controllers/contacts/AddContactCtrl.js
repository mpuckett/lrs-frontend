function AddContactCtrl($scope, ContactService, CandidateService, NotificationService, Options) {

	$scope.candidate = CandidateService.get({
		personID: $scope.data.personID
	}).then(function (response) {
		return response.data;
	});
	$scope.contact = {
		personID: $scope.candidate.personID,
		time: moment().format('MM/DD/YYYY')
	};
	$scope.appointment = {
		personID: $scope.candidate.personID,
		time: moment().add('days', 1).add('hours', 1).startOf('hour').format('MM/DD/YYYY HH:mm')
	};
	$scope.loading = false;

	$scope.options = Options;

	var success = function (response) {
		$scope.loading = false;
		$scope.addContactForm.showErrors = false;
		($scope.deferred && $scope.deferred.resolve || angular.noop)(response);
	};
	var failure = function (response) {
		$scope.loading = false;
		($scope.deferred && $scope.deferred.reject || angular.noop)(response);
	};

	$scope.submit = function () {
		if ($scope.addContactForm.$valid) {
			$scope.loading = true;
			// $q?
			// TODO: Check if appointment
			ContactService.create({
				data: angular.extend(angular.copy($scope.contact), {
					time: moment($scope.contact.time, 'MM/DD/YYYY').utc().format('YYYY-MM-DDTHH:mm:ssZZ')
				})
			}).then(function (response) {
				AppointmentService.create({
					data: angular.extend(angular.copy($scope.appointment), {
						time: moment($scope.appointment.time, 'MM/DD/YY HH:ss').utc().format('YYYY-MM-DDTHH:mm:ssZZ')
					})
				}).then(function (response) {
					NotificationService.reportSuccess(angular.extend(response.config, {
						successMessage: {
							type: 'Add Contact and Appointment',
							title: 'Contact and Appointment Added',
							description: 'Congratulations! You successfully added a contact and an appointment.'
						}
					}));
					success(response);
				}, failure);
			}, failure);
		} else {
			$scope.addContactForm.showErrors = true;
		}
		
	}

}

//