function DashboardCtrl($scope, $q, SessionService, AppointmentService, EventService, Contacts, Appointments, Events, Goals, Qualified, Unscreened, GoGuard, Generated, Contracts) {

	var isInPast = function (time) {
		// do not equalize (absolute time)
		return moment().valueOf() > moment(time, 'YYYY-MM-DDTHH:mm:ssZZ').valueOf();
	};

	var isAfterToday = function (time) {
		// equalize to compare
		return moment(time, 'YYYY-MM-DDTHH:mm:ssZZ').startOf('day').valueOf() >= moment().startOf('day').add('day', 1).valueOf();
	};

	var isThisWeek = function (time) {
		//equalize to compare
		return moment(time, 'YYYY-MM-DDTHH:mm:ssZZ').day(0).startOf('day').valueOf() === moment().day(0).startOf('day').valueOf();
	};

	var isThisMonth = function (time) {
		// equalize to compare
		return moment(time, 'YYYY-MM-DDTHH:mm:ssZZ').startOf('month').valueOf() === moment().startOf('month').valueOf();
	};

	var isOnFilteredDate = function (time) {
		// equalize to compare
		return moment(time, 'YYYY-MM-DDTHH:mm:ssZZ').startOf('day').valueOf() >= moment($scope.dateFilter, 'MM/DD/YYYY').startOf('day').valueOf();
	};

	var contactsThisMonth = _.filter(Contacts, function (contact) { return isThisMonth(contact.time); });
	var contactsThisWeek = _.filter(contactsThisMonth, function (contact) { return isThisWeek(contact.time); });

	var appointmentsThisMonth = _.filter(Appointments, function (appointment) { return isThisMonth(appointment.time); });
	var appointmentsThisWeek = _.filter(appointmentsThisMonth, function (appointment) { return isThisWeek(appointment.time); });

	var conductedAppointments = _.filter(Appointments, function (appointment) { return isInPast(appointment.time); });

	// TODO: LastAction?
	var contractsThisMonth = _.filter(Contracts, function (contract) { return isThisMonth(contract.lastAction); });
	var contractsThisWeek = _.filter(Contracts, function (contract) { return isThisWeek(contract.lastAction); });

	$scope.summary = {
		qualified: Qualified.length,
		unscreened: Unscreened.length,
		goGuard: GoGuard.length,
		generated: Generated.length
	};

	$scope.actual = {
		monthly: {
			contacts: contactsThisMonth.length,
			scheduled: appointmentsThisMonth.length,
			conducted: _.intersection(conductedAppointments, contactsThisMonth).length,
			contracts: contractsThisMonth.length
		},
		weekly: {
			contacts: contactsThisWeek.length,
			scheduled: appointmentsThisWeek.length, 
			conducted: _.intersection(conductedAppointments, contactsThisWeek).length,
			contracts: contractsThisWeek.length
		}
	};

	$scope.goals = Goals;

	$scope.date = moment().format('dddd, MMMM D, YYYY');

	$scope.dateFilter = moment().add('days', 1).format('MM/DD/YYYY');
	
	var setItemType = function (items, type) {
		_.each(items, function (item) {
			item.itemType = type;
		})
		return items;
	};

	$scope.isAppointment = function (item) {
		return item.itemType === 'appointment';
	};

	$scope.isEvent = function (item) {
		return item.itemType === 'event';
	};

	// resolved in router
	$scope.eventsAndAppointments = _.union(
		setItemType(Appointments, 'appointment'),
		setItemType(Events, 'event')
	);

	$scope.addEvent = function () {
		EventService.add().then(function (response) {
			angular.extend(response.data, response.config.data, { itemType: 'event' });
			$scope.eventsAndAppointments.push( response.data );
		});
	};

	$scope.orderByTime = function (item) {
		// sort based on the numerical value of the datestamp
		return moment(item.time, 'YYYY-MM-DDTHH:mm:ssZZ').valueOf();
	};

	$scope.filterTodayItems = function (item) {
		// equalize to compare
		return moment().startOf('day').valueOf() === moment(item.time, 'YYYY-MM-DDTHH:mm:ssZZ').startOf('day').valueOf();
	};

	$scope.filterUpcomingItems = function (item) {
		var dateFilterSet = $scope.dateFilter.length > 0; 
		return isAfterToday(item.time) &&
			dateFilterSet ? isOnFilteredDate(item.time) : true;
	};
	
}