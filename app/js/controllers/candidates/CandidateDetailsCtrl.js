function ContactDetailsCtrl($scope, ContactDetails, CandidateService) {
	$scope.candidate = CandidateService.get({ personID: $scope.data.personID }).then(function (response) {
		return response.data;
	});
	$scope.contacts = ContactDetails.get({ personID: $scope.data.personID }).then(function (response) {
		return response.data;
	});
}