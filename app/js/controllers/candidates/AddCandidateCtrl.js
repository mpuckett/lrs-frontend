function AddCandidateCtrl($scope, CandidateService, $rootScope, $route, CandidateType, ContactService, NotificationService, Options) {
	
	// TODO: Update Controller so it could work with either Add or Edit?
	// TODO: Can a candidate be edited at all?
	// TODO: SEPARATE OUT CALL TO ADD LEAD AND CALL TO ADD CONTACT

	// declare manage type for shared partial
	$scope.manageType = {
		name: 'Add'
	};

	$scope.candidateType = CandidateService.candidateTypes[CandidateType];
	$scope.options = Options;

	$scope.candidate = {
		contact: {}
	};

	var success = function (response) {
		$scope.loading = false;
		$scope.candidate = {}; // clear form
		$scope.form.showErrors = false;
		return response;
	};

	var failure = function (response) {
		$scope.loading = false;
		$scope.form.showErrors = false;
		return response;
	};

	$scope.submit = function () {
		if ($scope.form.$valid) {
			$scope.loading = true;
			CandidateService.create({ data: angular.copy($scope.candidate) }).then(success, failure); // save note
		} else {
			$scope.form.showErrors = true;
		}
	}

	// required for custom validation
	$scope.primaryContact = {};

	$scope.validateContact = function () {
		// return _.isEmpty($scope.candidate.contact); // doesn't quite cut it
		var hasLength = _.chain($scope.candidate.contact).toArray().compact().value().length > 0;
		return hasLength;
	};

}