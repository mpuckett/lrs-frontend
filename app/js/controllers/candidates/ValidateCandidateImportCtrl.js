function ValidateCandidateImportCtrl ($scope, $rootScope, CandidateService) {
	$scope.next = function () {
		$scope.loading = true;
		CandidateService.importProcess({
			data: angular.copy($scope.file)
		}).then(function (response) {
			// after notification
			$scope.reset();
		}, function (response) {
			$scope.reportError(response);
		}).then(function () {
			$scope.loading = false;
		});
	};
}