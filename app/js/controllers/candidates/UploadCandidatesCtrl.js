function UploadCandidatesCtrl ($scope, $rootScope, Options, CandidateService, RecruiterService) {

	// Events

	$scope.options = Options;

	// RSID assignment select box

	$scope.recruiters = [];

	RecruiterService.query().then(function (response) {
		var recruiters = response.data;
		_.each(recruiters, function (recruiter) {
			// for select box display
			recruiter.label = recruiter.rsid + ' - ' + recruiter.firstName + ' ' + recruiter.lastName;
		});
		$scope.recruiters = recruiters;
	});

	// File upload

	var validFileTypes = ['csv','xls','xslx'];

	$scope.validateFile = function (file) {
		var fileType = file && file.split('.').pop();
		return _.contains(validFileTypes, fileType);
	};

	$scope.setFileLocation = function (file) {
		// event handler for customized ng-change on file input
		$scope.$apply(function() {
			// reject if invalid filetype
			$scope.file = $scope.validateFile(file) ? file : null;
		});
	};

	// Submit form

	$scope.next = function () {
		if (!$scope.leadsImportForm.$invalid && $scope.file) {
			$scope.leadsImportForm.showInvalidInfo = false;
			$scope.loading = true; // TODO loader
			CandidateService.importUpload({
				form: $scope.leadsImportForm,
				data: angular.copy($scope.leadsImport)
			}).then(function (response) {
				$scope.nextStep(response.data);
			}, function (response) {
				$scope.reportError(response);
			}).then(function () {
				$scope.loading = false;
			});
		} else {
			$scope.leadsImportForm.showInvalidInfo = true;
			$scope.leadsImportForm.showErrors = true;
		}
		
	};

}