function SearchCandidatesCtrl($scope, $route, CandidateService, SavedSearchService, ContactService, View, CandidateType, SavedSearches, Options) {

	// TODO: AUTOCOMPLETE

	// use router to determine candidate type
	$scope.candidateType = CandidateService.candidateTypes[CandidateType];

	// use router to determine thumbs or list view in an ng-switch
	$scope.view = View;
	$scope.options = Options;
	$scope.savedSearches = SavedSearches;
	$scope.orderProp = ['','',''];
	$scope.orderPropReversed = [false, false, false];
	$scope.limit = 25;
	$scope.search = {};
	$scope.paging = {};

	// ACTIONS

	var success = function () {
		$scope.loading = false;
	};

	var failure = function () {
		$scope.loading = false;
	};

	// on search for leads
	$scope.doSearch = function (page) {
		$scope.loading = true;
		CandidateService.query({
			params: angular.extend(angular.copy($scope.search), {
				page: page || $scope.paging.currentPage || 1,
				limit: $scope.limit || 25,
				vilpacStatus: $scope.candidateType.vilpacStatus
			})
		}).then(function (response) {
			$scope.candidates = response.data;
			$scope.paging = response.paging;
		}).then(function () {
			$scope.loading = false;
		});
	};

	// on select saved search, populate search field
	$scope.updateSearch = function (search) {
		$scope.search = angular.copy(search); // shallow copy
		$scope.doSearch();
	};

	// start process for adding a saved search
	$scope.saveSearch = function () {
		SavedSearchService.add(angular.copy($scope.search))
			.then(function (response) {
				angular.extend(response.data, response.config.data);
				$scope.savedSearches.push(response.data); // TODO: Sort/order? alpha? date?
				$scope.savedSearch = response.data;
			});
	};
	$scope.deleteSearch = function (search) {
		SavedSearchService.remove({
			data: angular.copy(search)
		}).then( function (response) {
			$scope.savedSearches.splice( _.indexOf($scope.savedSearches, search), 1); // IE8 fix
		});
	};
	$scope.renameSearch = function (search) {
		SavedSearchService.rename({
			id : search.id,
			data : angular.copy(search)
		});
	};
	// start process for adding a contact associated with a candidate
	$scope.addContact = function (personID) {
		ContactService.add({ personID: personID });
	};

	// show modal for contact details associated with a candidate
	$scope.showContactDetails = function (personID) {
		CandidateService.showContactDetails({ personID: personID });
	};


	// TODO: Remove
	$scope.toggleOrderProp = function (i) {
		if ( $scope.orderProp[i].indexOf('-') === 0 ) {
			// make ascending
			$scope.orderProp[i] = $scope.orderProp[i].split('-')[1];
		} else {
			// make descending
			$scope.orderProp[i] = '-' + $scope.orderProp[i];
		}
		// toggle show/hide of ascending or descending <select>
		$scope.orderPropReversed[i] = !$scope.orderPropReversed[i];
	};


	$scope.$watch('search', function(){
		// if the user makes edits
		// after the search form has been populated
		if ( $scope.savedSearch && !angular.equals($scope.savedSearch, $scope.search) ) {
			// clear out saved search select box
			$scope.savedSearch = {};
		}
	}, true); // true for watching content of an object
}