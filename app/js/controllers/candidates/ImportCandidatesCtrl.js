function ImportCandidatesCtrl ($scope, $rootScope, Options, CandidateService, NotificationService) {

	// We will be passing $scope.file around between the views (import is a reserved term)

	$scope.file = null;

	// Navigation between views
	
	var i = 0;
	var views = ['upload', 'map', 'validate'];

	$scope.view = views[i];

	$scope.nextStep = function (file) {
		$scope.file = file; // can't directly update a parent $scope variable
		i++;
		$scope.view = views[i];
	};

	$scope.reset = function () {
		$scope.file = null;
		i = 0;
		$scope.view = views[i];
	};

	// For server errors

	$scope.reportError = function (response) {
		$scope.reset();
		$scope.showInvalidInfo = true;
	};

}