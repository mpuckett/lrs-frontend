function MapImportCandidateCtrl ($scope, $rootScope, Options, CandidateService) {

	// Required fields

	$scope.requiredFields = [
		"First Name",
		"Last Name",
		"Email",
		"SSN",
		"Phone",
		"Address",
		"City",
		"State",
		"ZIP",
		"Preferred Contact Method",
		"Highest Grade",
		"Gender",
		"Prior Service",
		"Date Contacted",
		"Notes"
	];

	// Initial mapping

	$scope.fileFields = $scope.file.columns;
	var preMappedFields = _.intersection($scope.requiredFields, $scope.fileFields);
	$scope.unmappedFileFields = _.difference($scope.fileFields, preMappedFields);
	$scope.unmappedRequiredFields = _.difference($scope.requiredFields, preMappedFields);
	$scope.mappedFields = {};

	_.each(preMappedFields, function (field) {
		$scope.mappedFields[field] = field;
	});

	// Modifiy mapping

	$scope.unmap = function (requiredField, fileField) {
		delete $scope.mappedFields[requiredField];
		$scope.unmappedRequiredFields.push(requiredField);
		$scope.unmappedFileFields.push(fileField);
	};

	$scope.map = function () {
		if (!$scope.mapForm.$invalid) {
			$scope.mappedFields[ $scope.unmapped.requiredField ] = $scope.unmapped.fileField;
			$scope.unmappedRequiredFields.splice( _.indexOf( $scope.unmappedRequiredFields, $scope.unmapped.requiredField), 1 );
			$scope.unmappedFileFields.splice( _.indexOf( $scope.unmappedFileFields, $scope.unmapped.fileField), 1 );
			$scope.unmapped = {};
		}
	};

	// Submit mapping

	$scope.next = function () {
		if ($scope.unmappedRequiredFields.length === 0) {
			$scope.loading = true; // TODO loader
			CandidateService.importMap({
				data: angular.extend($scope.file, {
					mappedColumns: $scope.mappedFields
				})
			}).then(function (response) {
				CandidateService.importValidate({
					data: angular.copy($scope.file)
				}).then(function (response) {
					$scope.nextStep(response.data);
				});
			}, function (response) {
				$scope.reportError();
			}).then(function () {
				$scope.loading = false;
			});
		} else {
			$scope.mapForm.showInvalidInfo = true;
			$scope.mapForm.showErrors = true;
		}
	};

}