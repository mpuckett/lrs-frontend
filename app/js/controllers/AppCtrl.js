function AppCtrl ($scope, $rootScope, SessionService) {
	$rootScope.$on('changeUser', function (e, session) {
		console.log('changeUser', session);
		$scope.user = session;
	});
	$scope.currentYear = new Date().getFullYear();
	$scope.logout = function () {
		SessionService.logout();
	};
	$scope.showToolbox = function () {
		$rootScope.$broadcast('showToolbox');
	};
}