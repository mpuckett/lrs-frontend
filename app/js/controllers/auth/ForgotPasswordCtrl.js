function ForgotPasswordCtrl($scope, $rootScope, SessionService) {
	$scope.submit = function () {
		$scope.forgotPasswordForm.showErrors = true;
		if ($scope.forgotPasswordForm.$valid) {
			$scope.loading = true;
			$scope.forgotPasswordForm.showSuccess = false;
			SessionService.retrievePassword({
				data: angular.copy($scope.email)
			}).then(function (response) {
				// success
				$scope.forgotPasswordForm.showSuccess = true;
				$scope.forgotPasswordForm.showErrors = false;
			}).then(function () {
				$scope.loading = false;
				$scope.email = null;
			});
		}
	};
}