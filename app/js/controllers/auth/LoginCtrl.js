function LoginCtrl($scope, $route, $rootScope, NotificationService, SessionService, API) {

	// 404 Request Errors etc. won't be shown on this page so error handling must be explicit

	// attempt CAC card login automatically on every other page but not this one...

	$scope.login = {};

	// TODO: need to logout?
	// SessionService.logout();
	// SessionService.login();

	// should happen in sessionservice
	// SessionService.login();

	// TODO: loaders

	$scope.doLogin = function () {
		if ($scope.loginForm.$valid) {
			$scope.loginForm.showInvalidInfo = false;
			$scope.loginForm.loading = true;
			SessionService.login({
				data: angular.copy($scope.login)
			}).then(angular.noop, function (response) {
				// error
				$scope.login = {};
				$scope.loginForm.showErrors = true;
				$scope.loginForm.showInvalidInfo = true;
			}).then(function () {
				$scope.loginForm.loading = false;
			});
		} else {
			$scope.loginForm.showErrors = true;
		}
	};

	$scope.doRegister = function () {
		if ($scope.registerForm.$valid) {
			$scope.registerForm.showInvalidInfo = false;
			$scope.registerForm.loading = true;
			SessionService.startRegister({
				data: angular.copy($scope.register)
			}).then(angular.noop, function (response) {
				// $scope.register = {};
				$scope.registerForm.showErrors = true;
				$scope.registerForm.showInvalidInfo = true;
			}).then(function () {
				$scope.registerForm.loading = false;
			});
		} else {
			$scope.registerForm.showErrors = true;
		}
	};

	$scope.cacLogin = function () {
		$scope.cacLoginForm.loading = true;
		$scope.cacLoginForm.showInvalidInfo = false;
		SessionService.login().then(angular.noop, function (response) {
			// error
			$scope.cacLoginForm.showInvalidInfo = true;
		}).then(function () {
			$scope.cacLoginForm.loading = false;
		});
	};

}