function GoalsCtrl($scope, Goals, GoalService) {
	$scope.goals = angular.copy(Goals);
	var toggleEditable = function () {
		$scope.isEditable = !$scope.isEditable;
	};
	var callback = function (response) {
		$scope.loading = false;
		toggleEditable();
	};
	$scope.sendToNCOIC = function () {
		$scope.loading = true;
		GoalService.send().then(function (response) {
			$scope.loading = false;
		});
	};
	$scope.submit = function () {
		// if not editable or no changes have been made, do not post to server
		if (!$scope.isEditable || angular.equals($scope.goals, Goals) ) {
			toggleEditable();
		} else {
			// otherwise validate and submit
			if ($scope.form.$valid) {
				$scope.loading = true;
				GoalService.update({ data: angular.copy($scope.goals) }).then(function (response) {
					Goals = response.config.data; // set new standard for evaluating changes
					callback(response);
				}, callback);
			} else {
				$scope.form.showErrors = true;
			}
		}
	};

	var setDate = function (date) {

		// accept moment object or string?

		$scope.currentMonth = date;
		$scope.previousMonth = moment($scope.currentMonth).subtract('months', 1);
		// $scope.nextMonth = moment($scope.currentMonth).add('month', 1);

		var daysInMonth = parseInt( moment($scope.currentMonth).endOf('month').format('D'),10); // 28
		var daysInPreviousMonth = parseInt( moment($scope.previousMonth).endOf('month').format('D'),10);
		var firstDayOfMonth = parseInt(moment($scope.currentMonth).startOf('month').format('d'),10); // 5 == thursday
		var daysRemaining = 7 - ( (daysInMonth + firstDayOfMonth) % 7);

		$scope.daysInMonth = [];
		for (i=0;i<daysInMonth;i++) {
			$scope.daysInMonth.push({
				date: $scope.currentMonth.date(i+1).valueOf(),
				display: $scope.currentMonth.date(i+1).format( i < 7 - firstDayOfMonth ? 'ddd D' : 'D'),
				unavailable: true
			});
		}
		$scope.daysOffset = [];
		for (i=0;i<firstDayOfMonth;i++) {
			$scope.daysOffset.push( $scope.previousMonth.date(daysInPreviousMonth - firstDayOfMonth + i + 1).format('ddd D') );
		}
		$scope.endOffset = [];
		for (i=0;i<daysRemaining;i++) {
			$scope.endOffset.push( i+1 );
		}
	};

	setDate(moment());

	$scope.test = function (day) {
		alert( moment(day).format('YYYY-MM-DD') );
	};

	$scope.prevMonth = function () {
		setDate(moment($scope.currentMonth).subtract('months', 1));
	};

	$scope.nextMonth = function () {
		setDate(moment($scope.currentMonth).add('months', 1));
	};

	$scope.setDate = function (date) {
		setDate(moment(date, 'MM/DD/YYYY'));
	};
}