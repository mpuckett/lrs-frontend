function SearchAssetsCtrl($scope, $rootScope, AssetService) {

	$scope.query = '';
	$scope.bookmarkFilter = '';
	$scope.tagFilter = '';

	$scope.saveItem = function (asset, form) {
		if (form.$valid) {
			asset.editable = false;
			if (form.$dirty) {
				AssetService.save({
					id: asset.id,
					data: angular.copy(asset),
					failure: $scope.failure
				});
			}
		} else {
			form.showErrors = true;
		}
	};

	$scope.toggleBookmark = function (asset) {
		asset.bookmark = !asset.bookmark;
		AssetService.toggleBookmark({
			id: asset.id,
			data: angular.copy(asset),
			failure: $scope.failure
		});
	};

	$scope.removeAsset = function (asset) {
		AssetService.remove({
			id: asset.id,
			failure: $scope.failure
		}).then(function (response) {
			$scope.assets.splice( _.indexOf($scope.assets, asset), 1); // IE8 FIX
		});
	};

	$scope.updateTagFilter = function (tag) {
		$scope.tagFilter = tag;
	};

	$scope.toggleBookmarkFilter = function () {
		$scope.bookmarkFilter = !$scope.bookmarkFilter ? 'true' : '';
	};

}