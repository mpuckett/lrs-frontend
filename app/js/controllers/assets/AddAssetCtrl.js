function AddAssetCtrl($scope, AssetService) {
	$scope.asset = {};
	$scope.getFile = function () {
		$scope.asset.file = prompt('File?');
	}
	$scope.getUrl = function () {
		$scope.asset.url = prompt('URL?');
	}
	$scope.submit = function () {
		if ($scope.addAssetForm.$valid) {
			AssetService.create({
				data: angular.copy($scope.asset),
				ctrlSuccess: function () {
					$scope.asset = {};
					$scope.addAssetForm.showErrors = false;
				}
			});
		} else {
			$scope.addAssetForm.showErrors = true;
		}
	}
}