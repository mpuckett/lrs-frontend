function ToolboxCtrl($scope, $rootScope, AssetService, SessionService) {
	
	$scope.shown = false;

	$rootScope.$on('showToolbox', function () {
		$scope.shown = true;
	});

	$scope.close = function () {
		$scope.shown = false;
	};

	$scope.makeEditable = function (asset) {
		if ($scope.isAdmin) {
			asset.editable = true;
		}
	};

	// TODO
	// can't open a modal within another modal
	// must specify toolbox-specific success error callback
	// var failure = function (error) {
	// 	alert('failure');
	// }

	$rootScope.$on('changeUser', function (e, session) {
		$scope.isAdmin = session.isAdmin;
		// AssetService.query().then(function (response) {
		// 	$scope.tags = response.data.tags || [];
		// 	$scope.tags.unshift(''); // add an empty string as first value
		// 	$scope.assets = response.data.assets;
		// });
	});

	

};