function AddEventCtrl($scope, EventService, NotificationService) {

	var success = function (response) {
		$scope.event = {};
		$scope.loading = false;
		($scope.deferred && $scope.deferred.resolve || angular.noop)(response);
	};
	var failure = function (response) {
		$scope.event = {};
		$scope.loading = false;
		($scope.deferred && $scope.deferred.reject || angular.noop)(response);
	};
	// TODO: Move all moment logic out of services
	// TODO: Move combined add of contacts into controller
	// TODO: Turn moment strings into variables
	$scope.submit = function () {
		if ($scope.addEventForm.$valid) {
			$scope.loading = true;
			EventService.create({
				data: angular.extend(angular.copy($scope.event), {
					time: moment($scope.event.time, 'MM/DD/YYYY HH:mm').utc().format('YYYY-MM-DDTHH:mm:ssZZ')
				})
			}).then(success, failure);
		} else {
			$scope.addEventForm.showErrors = true;
		}
	};
	$scope.event = {
		private: false,
		time: moment().add('days',1).add('hours',1).startOf('hour').format('MM/DD/YYYY HH:mm')
	};
}