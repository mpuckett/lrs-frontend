function LoaderCtrl($scope, $rootScope, $timeout, NotificationService) {

    var target = document.getElementById('spinner');
    var spinner = new Spinner().spin(target);

	$scope.shown = true;

	// $scope.started;

	$rootScope.$on('$routeChangeStart', function () {
		// console.log('routeChangeStart');
		// only show spinner if request is taking longer than .5 seconds
		// $scope.started = $timeout(function () {
		// 	console.log('starting loader');
		// 	$scope.shown = true;
		// 	return true;
		// }, 0.5*1000);
	});
	$rootScope.$on('$routeChangeSuccess', function (s) {
		// alert('success');
		// console.log('routeChangeSuccess');
		// $timeout.cancel($scope.started); // reject
		$scope.shown = false;
		// $scope.started.then(function () {
		// 	$scope.shown = false;
		// });
		
	});
	$rootScope.$on('$routeChangeError', function (rejection) {
		// console.log('routeChangeError');
		// $timeout.cancel($scope.started);
		// $scope.shown = false;
		// // NotificationService.reportFailure(rejection);
		// console.log('error');
	});
}