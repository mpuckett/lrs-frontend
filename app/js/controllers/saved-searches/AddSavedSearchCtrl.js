function AddSavedSearchCtrl($scope, SavedSearchService, NotificationService) {

	var search = angular.copy($scope.data.search);
	delete search.name; // remove name in case saving over a previous saved search
	$scope.search = search;

	var success = function (response) {
		$scope.search = {};
		($scope.deferred && $scope.deferred.resolve || angular.noop)(response);
	}
	var failure = function (response) {
		$scope.search = {};
		($scope.deferred && $scope.deferred.reject || angular.noop)(response);
	}
	$scope.submit = function () {
		if ($scope.addSavedSearchForm.$valid) {
			SavedSearchService.create({
				data: $scope.search
			}).then(success, failure);
		} else {
			$scope.addSavedSearchForm.showErrors = true;
		}
		
	}
}