'use strict';

/* Filters */

angular.module('lrsFilters', []).
    filter('onOff', function () {
        return function (text) {
        	return text ? 'on' : 'off';
        }
    }).
    filter('yN', function () {
        return function (text) {
        	return text ? 'Y' : 'N';
        }
    }).
    filter('moment', function () {
        return function (date, value) {
            return moment(date).format(value);
        }
    }).
    filter('translate', function (Options) {
        return function (text, set) {
            var item = _.where(Options[set], { value: text });
            var isInSet = item.length > 0;
            return isInSet ? item[0].label : '';
        }
    }).
    filter('paragraphs', function () {
        return function (text) {
            return text ? text.replace(/\n/gi,'</p><p>') : '';
        }
    }).
    filter('tel', function () {
        // http://stackoverflow.com/questions/12700145/how-to-format-a-telephone-number-in-angularjs
        return function (tel) {
            if (!tel) { return ''; }

            var value = tel.toString().trim().replace(/^\+/, '');

            if (value.match(/[^0-9]/)) {
                return tel;
            }

            var country, city, number;

            switch (value.length) {
                case 10: // +1PPP####### -> C (PPP) ###-####
                    country = 1;
                    city = value.slice(0, 3);
                    number = value.slice(3);
                    break;

                case 11: // +CPPP####### -> CCC (PP) ###-####
                    country = value[0];
                    city = value.slice(1, 4);
                    number = value.slice(4);
                    break;

                case 12: // +CCCPP####### -> CCC (PP) ###-####
                    country = value.slice(0, 3);
                    city = value.slice(3, 5);
                    number = value.slice(5);
                    break;

                default:
                    return tel;
            }

            if (country === 1) {
                country = '';
            }

            number = number.slice(0, 3) + '-' + number.slice(3);

            return (country + ' (' + city + ') ' + number).trim();
        };
    });
    